/*
SQLyog Community v11.21 (32 bit)
MySQL - 5.5.32 : Database - babi_ci
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `babi_categories` */

DROP TABLE IF EXISTS `babi_categories`;

CREATE TABLE `babi_categories` (
  `category_id` int(11) NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `status` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `babi_categories` */

insert  into `babi_categories`(`category_id`,`category`,`parent_id`,`status`,`id_path`) values (255,' Quần Áo Bé Trai',0,'A','255'),(257,'Áo sơ mi bé trai',255,'A','255/257'),(258,'Quần Áo Sơ Sinh',0,'A','258'),(259,'Phụ kiện sơ sinh cho bé',258,'A','258/259'),(263,'Áo thun bé trai',255,'A','255/263'),(265,'Quần kiểu bé trai',255,'A','255/265'),(266,'Quần jeans bé trai',255,'A','255/266'),(267,'Đồ bộ bé trai',255,'A','255/267'),(268,'Đồ bơi bé trai',255,'A','255/268'),(269,'Quần Áo Bé Gái',0,'A','269'),(271,'Váy Đầm bé gái',269,'A','269/271'),(272,'Áo bé Gái',269,'A','269/272'),(273,'Quần trẻ em gái',269,'A','269/273'),(274,'Đồ bộ bé gái',269,'A','269/274'),(275,'Đồ bơi bé gái',269,'A','269/275'),(287,'Phụ kiện',0,'A','287'),(288,'Đồ chơi trẻ em',0,'H','288'),(333,'Quần Chip trẻ em',287,'A','287/333'),(344,'Hàng cũ',0,'H','344'),(345,'Áo Mưa trẻ em',287,'A','287/345'),(346,'Cập Nhật Số Lượng Mới Nhất',0,'H','346'),(347,'Giày-Dép trẻ em',287,'A','287/347'),(349,'Mắt Kính trẻ em',287,'A','287/349'),(350,'Kẹp Tóc trẻ em',287,'A','287/350'),(351,'Chăn - Khăn em bé',287,'A','287/351'),(352,'Ba lô trẻ em- Túi đựng đồ cho mẹ',287,'A','287/352'),(353,'Tất Chân trẻ em',287,'A','287/353'),(354,'Nón - Băng Đô - Khăn Choàng Cổ',287,'A','287/354'),(355,'Áo khoác bé trai',255,'A','255/355'),(356,'Áo khoác bé gái',269,'A','269/356'),(357,'Quần áo sơ sinh bé trai',258,'A','258/357'),(358,'Quần áo sơ sinh bé gái',258,'A','258/358'),(361,'Hàng mới về',0,'A','361'),(364,'Đồ bộ Baby Gap bé trai',255,'H','255/364'),(365,'Đồ bộ Baby Gap bé gái',269,'H','269/365'),(366,'Đồ dùng cho bé',0,'A','366'),(367,'Thực phẩm cho bé',366,'A','366/367'),(368,'Đồ dùng khác',366,'A','366/368'),(369,'Đồ chơi cho bé',366,'A','366/369'),(371,'Dầu Tắm Gội - Dưỡng Da Cho Bé',366,'A','366/371'),(372,'Chăm sóc răng miệng cho bé',366,'A','366/372'),(373,'Bình sữa - Núm Ti',366,'A','366/373'),(374,'Hàng Cũ 2013',0,'H','374'),(377,'test dm',0,'H','377'),(380,'Products',0,'H','380'),(382,'Vệ sinh bình sữa - giặt tẩy - giặt xả',366,'A','366/382'),(383,'Bỉm - Tã - Khăn ướt cho bé',366,'A','366/383'),(384,'Sản Phẩm Bán Chạy - Home',0,'H','384'),(385,'Quần áo sơ sinh (Home)',0,'H','385'),(386,'Quần áo bé trai (Home)',0,'H','386'),(387,'Quần áo bé gái (Home)',0,'H','387'),(388,'Phụ kiện thời trang (Home)',0,'H','388'),(389,'Khuyến Mãi',0,'H','389');

/*Table structure for table `babi_order_details` */

DROP TABLE IF EXISTS `babi_order_details`;

CREATE TABLE `babi_order_details` (
  `item_id` int(11) NOT NULL,
  `order_id` mediumint(8) NOT NULL,
  `product_id` mediumint(8) DEFAULT NULL,
  `product_code` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` decimal(12,0) DEFAULT NULL,
  `amount` smallint(5) DEFAULT NULL,
  `extra` text COLLATE utf8_unicode_ci,
  `product` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`item_id`,`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `babi_order_details` */

/*Table structure for table `babi_orders` */

DROP TABLE IF EXISTS `babi_orders`;

CREATE TABLE `babi_orders` (
  `order_id` int(11) NOT NULL,
  `timestamp` int(11) NOT NULL DEFAULT '0',
  `status` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `total` decimal(12,0) NOT NULL DEFAULT '0',
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `babi_orders` */

/*Table structure for table `babi_report` */

DROP TABLE IF EXISTS `babi_report`;

CREATE TABLE `babi_report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_start` int(11) NOT NULL DEFAULT '0',
  `date_end` int(11) NOT NULL DEFAULT '0',
  `date_start_str` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_end_str` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `month` tinyint(2) DEFAULT NULL,
  `year` smallint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `babi_report` */

insert  into `babi_report`(`id`,`title`,`date_start`,`date_end`,`date_start_str`,`date_end_str`,`month`,`year`) values (17,'Hoàn thành tháng 7',1389049200,0,'01/07/2014','31/07/2014',7,2014);

/*Table structure for table `babi_status` */

DROP TABLE IF EXISTS `babi_status`;

CREATE TABLE `babi_status` (
  `status_id` int(11) NOT NULL,
  `type` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'O',
  `status` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_default` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `babi_status` */

insert  into `babi_status`(`status_id`,`type`,`status`,`description`,`is_default`) values (16,'O','P','Đang giao','Y'),(17,'O','C','Hoàn Thành','Y'),(18,'O','O','Chờ duyệt','Y'),(19,'O','F','Lỗi','Y'),(20,'O','D','Từ Chối','Y'),(21,'O','B','Hàng trả về','Y'),(22,'O','I','Hủy','Y'),(31,'O','A','Soạn xong (1)','N'),(32,'O','E','Soạn hàng (1)','N'),(33,'O','G','Chỉnh sửa','N'),(34,'O','H','Kho xả hàng','N'),(35,'O','J','Đợi khách','N'),(37,'O','L','Babi giao','N'),(39,'O','M','CK-Gọi','N'),(40,'O','Q','CK-Chờ','N'),(41,'O','R','Soạn hàng (2)','N'),(42,'O','S','Offline','N'),(43,'O','U','Soạn xong (2)','N');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
