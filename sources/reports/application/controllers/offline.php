<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Offline extends MY_Controller {
	
	public $api_config;
	public $babi_status;
	public $categories;
	//filter
	//$type_filter = array();
	function __construct(){
		
		parent::__construct();
		
		$this->sections = array(
			array('title'=>'Products','url'=>'','action'=>'index'),
		);
		$this->data['menu_active'] = 'offline';
		$this->data['breadcrumbs']['offline'] = array('title'=>'Offline');
		
		$xml=simplexml_load_file("config.xml");
		$this->api_config = $xml->api;
		
		//babi_status
		//$this->babi_status = $this->db->get('babi_status')->result_array();
		//$this->data['babi_status'] = $this->babi_status;
		
		//babi_categories
		$this->categories = $this->db->get('babi_categories')->result_array();
		$babi_categories_array = array();
		foreach($this->categories as $cat){
			$babi_categories_array[$cat['category_id']] = $cat['category'];
		}
		$this->data['babi_categories'] = $babi_categories_array;
		//$this->categories = $babi_categories_array;
		//$this->data['babi_categories'] = $babi_categories_array;
		//print_r($this->data['babi_categories']);exit;
		$this->data['type_filter'] = array(
			''				=> '--------------',
			'group_product'	=> 'Thống kê số lượng bán từng sản phẩm (Product Id )',
			'group_product_code'	=> 'Thống kê số lượng bán từng combination (Product code )',
			//'group_day'	=> 'Thống kê số lượng bán từng ngày'
		);
		
		//$this->output->cache(60000);
		$this->load->library("excel");
		$this->load->helper("excel");
	}
	

	public function index( )
	{
		$file = $this->uri->uri_string();
		$post = $this->input->post();
		
		if(!empty($post['export_detail'])){
			$data_exp = array();
			$query = '';
			$ti = 'file';
			$t1 = "BÁO CÁO OFFLINE";
			$ti = $t1." từ ";
			$tbl = 'csv_order_details';
			$this->load->dbutil();
			$query = $this->db->last_query();
			
		
			
			$_query = $this->db->query($query);
			$delimiter = ",";
			$newline = "\r\n";
			$t = $ti.$newline;
			$content_csv  = $this->dbutil->csv_from_result($_query,$delimiter,$newline);

			
			//GIANG LAM EXCEL
			$this->array_symbol = array("A","B","C","D","E","F","G");
			$objPHPExcel = new PHPExcel();
			
			$objPHPExcel->setActiveSheetIndex(0);//don hang
			
			
			
			$list_title = array("Stt","Order id","Ngày","Số lần in","Total","Total thay đổi","In lần cuối");
			$symbol_width = array("A"=>10,"B"=>10,"C"=>12,"D"=>10,"E"=>14,"F"=>10); // chieu rong từng cot
			header_excel($list_title,$symbol_width,$objPHPExcel,2,true,true);
			$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(25);
			$row = 3;
			$row_start = $row;
			$array_symbol =$this->array_symbol;
			$row_start = 0;

			
			//TITLE
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,1,$t);
			
			$background = !empty($_REQUEST['background']) ? $_REQUEST['background'] : '1EC3C8';//'1EC3C8'; //1EC3C8   1155CC
			$font =  array('bold'=> true, 'color' => array('rgb'=> 'ffffff'),'size'  => 10);
			$styleArray = array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => $background)),'font'=>$font); // mau background
			
			
			$objPHPExcel->getActiveSheet()->mergeCells('A1:G1')->getStyle("A1")->applyFromArray($styleArray)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			
			//set màu sắc hết title
			$objPHPExcel->getActiveSheet()->getStyle("A1:G2")->applyFromArray($styleArray);
			
			//MERGE nao
			$objPHPExcel->getActiveSheet()->mergeCells('A2:A2')->getStyle("A2")->applyFromArray($styleArray)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->mergeCells('B2:B2')->getStyle("B2")->applyFromArray($styleArray)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->mergeCells('C2:C2')->getStyle("C2")->applyFromArray($styleArray)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			
			
						
			
			
			//TITLE2
			$font =  array('bold'=> true, 'color' => array('rgb'=> 'ffffff'),'size'  => 10);
			$styleArray = array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => $background)),'font'=>$font); // mau background
			$objPHPExcel->getActiveSheet()->getStyle("A2:G2")->applyFromArray($styleArray)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			
			
			//$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
			
			$doanh_so = array();
			
			#print_r($this->api_config);exit; ->url
			$time_from = $_REQUEST['time_from'];
			$time_to = $_REQUEST['time_to'];
			
			$url = (string)$this->api_config->url."orders3?time_from=$time_from&time_to=$time_to&status=C&total1=Y";
			
			$email = "hagiang@blinkb.com";
			$api_key = "1V8A3GvzH4mHUO08l099r4Ji5Aos78jO";
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_USERPWD, $email . ":" . $api_key);
			//curl_setopt($ch, CURLOPT_POSTFIELDS,$order_data);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json'
				//'Content-Length: 1')
			));

			$result = curl_exec($ch);
			#print_r($result);exit;
			$data = json_decode($result,JSON_FORCE_OBJECT);
			curl_close($ch);
			$orders = $data['orders'];
			$params = $data['params'];
			
			
			
			$data_time = array();
			if(!empty($params['time_from'])){
				
				$time_from = date("d/m/Y H:i",$params['time_from']);
				
				$time_to = date("d/m/Y H:i",$params['time_to']);
				$ti .= $time_from. ' - '. $time_to;
				$objPHPExcel->getActiveSheet()->mergeCells('A1:AY1')->setCellValueByColumnAndRow(0,1,$ti);
				#$time_test = strtotime('11/02/2014 23:59');
				#echo $time_test;echo '<br />';
			}
			else{
				echo "Chưa chọn thời gian";exit;
			}
			
			
			//print_r($data_time);exit;
			
			
			
			//foreach()
			
			
		
			
			
			$stt = 0;
			foreach($orders as $t){
				$stt++;	
					$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,$row,$stt);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1,$row,$t['order_id']);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2,$row,date("d/m/Y H:i",$t['timestamp']));
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3,$row,$t['total_print']);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4,$row,$t['total1']);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5,$row,$t['total2']);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6,$row,date("d/m/Y H:i",$t['time_last']));
					
				$i = 0;
				$row_start = $row;
				$row++;
				$objPHPExcel->getActiveSheet()->freezePane('D4');
			}
			$objPHPExcel->getActiveSheet()->getStyle('E4:E'.$row)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->getStyle('F4:F'.$row)->getNumberFormat()->setFormatCode('#,###');
			
			
			
			$objPHPExcel->getActiveSheet()->getStyle('F2:F'.$row)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->getStyle('G2:G'.$row)->getNumberFormat()->setFormatCode('#,###');
			$styleArray = array('font' => array('bold'=> false, 'color' => array('rgb1'=> '000'),'size'  => 10));
			$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':AG'.$row)->applyFromArray($styleArray);	
			
			//style
			$font =  array('bold'=> true, 'color' => array('rgb'=> '24A7F4'),'size'  => 10);
			$styleArray = array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'ccffcc')),'font'=>$font); // mau background
			//SUM tong don hang
			
			
			//MAU SAC CHO DE NHIN
			$_font =  array('size'  => 10);
			$_styleArray = array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'D8D8D8')),'font'=>$_font); // mau background
			for($j = 4;$j < $row; $j++){
				if($j%2 == 0){
					$objPHPExcel->getActiveSheet()->getStyle('A'.$j.':BD'.$j)->applyFromArray($_styleArray);
				}
			}
			
			header('Content-Type: application/vnd.ms-excel'); 
			header('Content-Disposition: attachment;filename="'.$ti.'.xls"'); 
			header('Cache-Control: max-age=0'); 
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5'); 
			$objWriter->save('php://output');
			return;
			//print_r($data_products);exit;
			 //= array_filter($data_employ);
			
			
			
			
			
			
			
			$file_download = './uploads/file_save.csv';
			$this->load->helper('file');
			write_file($file_download, $t.$content_csv);
			$this->load->helper('download');
			force_download($ti.'.csv',$t.$content_csv);
			
			//print_r($data_exp);
			exit;
		}
		
		//else{ // khi load trang
		
		$cat_ids = array();
		$cat_ids = $this->db->select('category_id,sum(amount) as quantity')->group_by('category_id')->distinct()->get('csv_order_details')->result_array();
		$cat_ids_arr = array();
		$cat_ids_all = array();
		
		$cat_ids_arr[] = 0;
		$cat_ids_all[0] = array('quantity'=>0);
		$qu = 0;
		foreach($cat_ids as $c){
			$cat_ids_arr[] = $c['category_id'];
			$cat_ids_all[$c['category_id']] = array('quantity'=>$c['quantity']);
			$qu += $c['quantity'];
		}
		$cat_ids_all[0] = array('quantity'=>$qu);
		
		$this->db->where_in('category_id',$cat_ids_arr);
		$_categories = $this->db->get('babi_categories')->result_array();
		$babi_categories_array = array();
		foreach($_categories as $cat){
			$babi_categories_array[$cat['category_id']] = $cat['category'] . ' - '.$cat_ids_all[$cat['category_id']]['quantity'];
		}
		$this->data['babi_categories'] = $babi_categories_array;
		//}
		//$this->data['babi_categories'] = $this->categories;
		//print_r($this->categories);exit;
		//echo $post['cat'];exit;
		$this->data['category_selected'] = !empty($post['cat']) ? $post['cat'] : '';
	
		
		
		$this->data['class_step3'] = 'hide';
		$this->data['active_child'] = 'offline';
		$this->data['header_title'] = 'Doanh số';
		$this->data['toolbars'] = array(
			array('title'=>'Add','desc'=>'Thêm mới sản phẩm','icon'=>'fa-plus-square','url'=>site_url('products/add')),
			array('title'=>'Refesh','desc'=>'Làm mới','icon'=>'fa-refresh','url'=>'#')
		);
		$this->set_css('css/datepicker.css');
		
		//$this->set_js('vendor/jquery-ui.custom.min.js');
		$this->set_js('js/bootstrap-datepicker.js');
		
		$hook_js = $this->build('offline/hooks/js/index',true);
		$html = $this->build('offline/table/list',true);
		$this->miniHTML($html);
		$this->miniHTML($hook_js);
		$this->data['content'] = $html;
		$this->data['hook_js'] = $hook_js;
		$this->set_css('vendor/datatables/jquery.dataTables.css');
		$this->set_js('vendor/bootstrap-select/bootstrap-select.js');
		$this->set_js('vendor/datatables/jquery.dataTables.js');
		//$this->set_js('vendor/fuelux/checkbox.js');
		$this->set_js('js/datatables.js');
//		$this->set_js('js/forms.js');
		$this->set_css('vendor/bootstrap-select/bootstrap-select.css');
		$this->build('index');
	}
	
	function miniHTML( &$html ){
		$html = str_replace("\n","",$html);
		$html = str_replace("\t"," ",$html);
		$html = str_replace("   "," ",$html);
		$html = str_replace("    "," ",$html);
		$html = str_replace("     "," ",$html);
		$html = str_replace(">  <","><",$html);
		$html = str_replace("> <","><",$html);
		$html = str_replace(">   ",">",$html);
		$html = str_replace("   <","<",$html);
		$html = str_replace("  <","<",$html);
		$html = preg_replace("/<!--.*?-->/ms","",$html);		
		$html = str_replace("\r","",$html);
		//$html = $this->delete_all_between('/*', '*/', $html);
	}
	/*function delete_all_between($beginning, $end, $string) {
	  $beginningPos = strpos($string, $beginning);
	  $endPos = strpos($string, $end);
	  if ($beginningPos === false || $endPos === false) {
		return $string;
	  }

	  $textToDelete = substr($string, $beginningPos, ($endPos + strlen($end)) - $beginningPos);

	  return str_replace($textToDelete, '', $string);
	}*/
}