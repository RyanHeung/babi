<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sales extends MY_Controller {
	
	public $api_config;
	public $babi_status;
	function __construct(){
		
		parent::__construct();
		$this->output->cache(60000);
		$this->sections = array(
			array('title'=>'Products','url'=>'','action'=>'index'),
		);
		$this->data['menu_active'] = 'sales';
		$this->data['breadcrumbs']['sales'] = array('title'=>'Sales');
		
		$xml=simplexml_load_file("config.xml");
		$this->api_config = $xml->api;
		//babi_status
		$this->babi_status = $this->db->get('babi_status')->result_array();
		$this->data['babi_status'] = $this->babi_status;
	}
	public function index()
	{
		$data_post['start'] = '';
		$data_post['end'] = '';
		$data_post['title'] = '';
		$data_post['class_step3'] = 'hide';
		$data_post['class_step2'] = 'hide';
		
		
		$post = $this->input->post();
		if(!empty($post)){
			$data_post['class_step2'] = '';
			
			$start = $post['start'];
			$end = $post['end'];
			$data_post['start'] = $post['start'];
			$data_post['end'] = $post['end'];
			
			$file_name = str_replace("/","_","Report_Orders_$start---$end");
			$data_post['title'] = !empty($post['title']) ? $post['title'] : $file_name;
			
			//status%5B%5D=
			$status_change = $post['status'];
			$status_query = "";
			foreach($status_change as $s){
				$status_query .= "&status%5B%5D=".$s;
				//checked $this->babi_status
				foreach($this->babi_status as $k=>&$v){
					if($v['status'] == $s){
						$v['checked'] = true;
					}
				}
			}
			$this->data['babi_status'] = $this->babi_status;
			
			
			//SAVE //LUU DU LIEU VAO LAYOUT DE TIM KIEM SAU NAY
			$order_ids = array();
			if(!empty($post['title']) && !empty($post['save'])){
				$_start = strtotime($start);
				$_end = strtotime($end);
				$url = (string)$this->api_config->orders."?is_search=Y&cname=&email=&b_phone=&issuer=&total_from=&total_to=&period=C&time_from=".$start."&time_to=".$end.$status_query;
				$email = (string)$this->api_config->email;
				$api_key = (string)$this->api_config->key;
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_USERPWD, $email . ":" . $api_key);
				//curl_setopt($ch, CURLOPT_POSTFIELDS,$order_data);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array(
					'Content-Type: application/json'
					//'Content-Length: 1')
				));

				$result = curl_exec($ch);
				
				$orders = json_decode($result,JSON_FORCE_OBJECT);
				curl_close($ch);
				$babi_report = array('date_start_str'=>$start,'date_end_str'=>$end,'title'=>$post['title'],'date_start'=>strtotime($start),'date_end'=>strtotime($end),'year'=>date("Y"),'month'=>date("m"));
				$this->db->insert('report',$babi_report);
				//babi_orders
				$j = 0;
				$order_total = 0;
				foreach($orders as $o){
					$order_ids[] = $o['order_id'];
					$this->db->where('order_id',$o['order_id']);
					$exist = $this->db->get('orders')->row_array();
					//echo count($orders);exit;
					if(empty($exist)){ // chua co
						$order = array(
							'order_id' 	=> $o['order_id'],
							'timestamp'	=> $o['timestamp'],
							'status'	=> $o['status'],
							'total'		=> $o['total']
						);
						$this->db->insert('orders',$order);
					}
					$order_total += $o['total'];
					$j++;
				}
				$j = number_format($j,0,',','.');
				$order_total = number_format($order_total,0,',','.');
				$this->data['notifications_form'][] = array('message'=>"Tổng đơn hàng: <strong>$j</strong>",'type'=>"S");//error
				$this->data['notifications_form'][] = array('message'=>"Tổng tiền : <strong>$order_total</strong> đ",'type'=>"S");//error
				$data_post['class_step3'] = '';
				/*
					//tim trong admin chua cho thi them | co roi thi UPDATE
					$this->db->where('order_id',$o['order_id']);
					$exist = $this->db->get('orders')->row_array();
					if(empty($exist)){ // chua co
						$order = array(
							'order_id' 	=> $o['order_id'],
							'timestamp'	=> $o['timestamp'],
							'status'	=> $o['status'],
							'total'		=> $o['total']
						);
						$this->db->insert('orders',$order);
					}
				}*/
			}
			//Luu chi tiet don hang su dung order_ids
			if(!empty($order_ids)){
				
				$order_ids_string = implode(",",$order_ids);
				//echo $order_ids_string;exit;
				$url = (string)$this->api_config->babi_orders."?order_ids=".$order_ids_string;
				$email = (string)$this->api_config->email;
				$api_key = (string)$this->api_config->key;
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_USERPWD, $email . ":" . $api_key);
				//curl_setopt($ch, CURLOPT_POSTFIELDS,$order_data);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array(
					'Content-Type: application/json'
					//'Content-Length: 1')
				));

				$result = curl_exec($ch);
				
				$order_data = json_decode($result,JSON_FORCE_OBJECT);
				$order_items = $order_data['items'];
				$k = 0;
				foreach($order_items as $item){
					$this->db->where('item_id',$item['item_id']);
					$this->db->where('order_id',$item['order_id']);
					$exist = $this->db->get('order_details')->row_array();
					
					if(empty($exist)){ // chua co
						//echo $item['item_id'];
						//print_r($exist);
						//echo 2;exit;
						//print_r($item);exit;
						$this->db->insert('order_details',$item);
					}
					$k += $item['amount'];
				}
				$k = number_format($k,0,',','.');
				$this->data['notifications_form'][] = array('message'=>"Tổng số lượng: <strong>$k</strong>",'type'=>"S");//error
				curl_close($ch);
			}
			//END:SAVE chi tiet don hang
			
			
			$url = (string)$this->api_config->orders."?is_search=Y&cname=&email=&b_phone=&issuer=&total_from=&total_to=&period=C&items_per_page=20&time_from=".$start."&time_to=".$end;
			$email = (string)$this->api_config->email;
			$api_key = (string)$this->api_config->key;
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_USERPWD, $email . ":" . $api_key);
			//curl_setopt($ch, CURLOPT_POSTFIELDS,$order_data);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json'
				//'Content-Length: 1')
			));

			$result = curl_exec($ch);
			
			$orders = json_decode($result,JSON_FORCE_OBJECT);
			curl_close($ch);
			$this->data['model'] = $orders;
			
			//print_r($orders);exit;
		}
		$this->data['start'] = $data_post['start'];
		$this->data['end'] = $data_post['end'];
		$file_name = str_replace("/","_","Report_Orders_".$this->data['start']."---".$this->data['end']);
		$this->data['title'] = $file_name;//$data_post['title'];
		$this->data['class_step2'] = $data_post['class_step2'];
		$this->data['class_step3'] = $data_post['class_step3'];
		$this->data['active_child'] = 'sales';
		$this->data['header_title'] = 'Sales';
		$this->data['toolbars'] = array(
			array('title'=>'Add','desc'=>'Thêm mới sản phẩm','icon'=>'fa-plus-square','url'=>site_url('products/add')),
			array('title'=>'Refesh','desc'=>'Làm mới','icon'=>'fa-refresh','url'=>'#')
		);
		$this->set_css('css/datepicker.css');
		
		//$this->set_js('vendor/jquery-ui.custom.min.js');
		$this->set_js('js/bootstrap-datepicker.js');
		
		$this->data['hook_js'] = $this->build('sales/hooks/js/index',true);
		$this->data['content'] = $this->build('sales/table/list',true);
		$this->set_css('vendor/datatables/jquery.dataTables.css');
		$this->set_js('vendor/bootstrap-select/bootstrap-select.js');
		$this->set_js('vendor/datatables/jquery.dataTables.js');
		$this->set_js('vendor/fuelux/checkbox.js');
		$this->set_js('js/datatables.js');
//		$this->set_js('js/forms.js');
		$this->set_css('vendor/bootstrap-select/bootstrap-select.css');
		$this->build('index');
	}
	
	public function add(){
		$this->data['content'] = $this->build('products/form',true);
		$this->data['hook_js'] = $this->build('products/hooks/js/form',true);
		$this->data['breadcrumbs']['products']['url'] = site_url('products');
		$this->data['breadcrumbs']['add'] = array('title'=>'Thêm mới');
		$this->data['toolbars'] = array(
			array('title'=>'Lưu','desc'=>'Lưu sản phẩm','icon'=>'fa-save','url'=>'#'),
			array('title'=>'Thoát','desc'=>'Làm mới','icon'=>'fa-undo','url'=>site_url('products'))
		);
		$this->set_js('ckeditor/ckeditor.js');
		
		$this->build('index');
	}
	public function edit( $id ){
	}
}