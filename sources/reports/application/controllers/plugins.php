<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Plugins extends MY_Controller {
	
	function __construct(){
		
		parent::__construct();
		$this->output->cache(60000);
		$this->sections = array(
			array('title'=>'Plugin','url'=>'','action'=>'index'),
		);
		$this->data['menu_active'] = 'plugins';
		$this->data['breadcrumbs']['plugins'] = array('title'=>'Plugins');
	}
	public function index()
	{
		//$this->data['active_child'] = 'products';
		$this->data['hook_js'] = $this->build('plugins/hooks/js/index',true);
		$this->data['header_title'] = 'Plugins';
		$this->data['toolbars'] = array(
			array('title'=>'Add','desc'=>'Add User','icon'=>'fa-plus-square','url'=>site_url('plugins/add')),
			array('title'=>'Refesh','desc'=>'Refesh','icon'=>'fa-refresh','url'=>'#')
		);
		
		$this->data['content'] = $this->build('plugins/table/list',true);
		$this->set_css('vendor/datatables/jquery.dataTables.css');
		//$this->set_js('vendor/datatables/jquery.dataTables.js');
		//$this->set_js('js/datatables.js');
		$this->build('index');
	}
	
	public function add(){
		$this->data['header_title'] = 'Information';
		$this->data['content'] = $this->build('plugins/form',true);
		$this->data['hook_js'] = $this->build('plugins/hooks/js/form',true);
		$this->data['breadcrumbs']['plugins']['url'] = site_url('plugins');
		$this->data['breadcrumbs']['add'] = array('title'=>'Add user');
		$this->data['toolbars'] = array(
			array('title'=>'Save','desc'=>'Save','icon'=>'fa-save','url'=>'#'),
			array('title'=>'Exit','desc'=>'Exit','icon'=>'fa-undo','url'=>site_url('users'))
		);
		$this->set_css('vendor/bootstrap-select/bootstrap-select.css');
		$this->set_js('vendor/bootstrap-select/bootstrap-select.js');
		$this->set_css('css/datepicker.css');
		$this->set_js('js/bootstrap-datepicker.js');
		
		
		$this->build('index');
	}
	public function edit( $id ){
	}
}