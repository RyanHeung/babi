<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report extends MY_Controller {
	
	public $api_config;
	public $babi_status;
	public $categories;
	//filter
	//$type_filter = array();
	function __construct(){
		
		parent::__construct();
		//$this->output->cache(60000);
		$this->sections = array(
			array('title'=>'Products','url'=>'','action'=>'index'),
		);
		$this->data['menu_active'] = 'report';
		$this->data['breadcrumbs']['report'] = array('title'=>'Report');
		
		$xml=simplexml_load_file("config.xml");
		$this->api_config = $xml->api;
		//babi_status
		//$this->babi_status = $this->db->get('babi_status')->result_array();
		//$this->data['babi_status'] = $this->babi_status;
		
		//babi_categories
		$this->categories = $this->db->get('babi_categories')->result_array();
		$babi_categories_array = array();
		foreach($this->categories as $cat){
			$babi_categories_array[$cat['category_id']] = $cat['category'];
		}
		$this->data['babi_categories'] = $babi_categories_array;
		//$this->categories = $babi_categories_array;
		//$this->data['babi_categories'] = $babi_categories_array;
		//print_r($this->data['babi_categories']);exit;
		$this->data['type_filter'] = array(
			''				=> '--------------',
			'group_product'	=> 'Thống kê số lượng bán từng sản phẩm',
			//'group_day'	=> 'Thống kê số lượng bán từng ngày'
		);
	}
	

	public function index( )
	{
		$file = $this->uri->uri_string();
		//$this->output->cache(60000);
		//echo base_url('report');exit;
		//echo md5(site_url('report'));exit;
		$this->data['category_selected'] = '';
		$this->data['type_filter_selected'] = '';
		$params = $this->input->get();
		
		$this->data['class_step2'] = 'hide';
		
		$folder = "./uploads";
		$config['upload_path'] = $folder;
		$config['allowed_types'] = '*';//doc|docx|zip|rar|jpg|png|xls|xlsx|csv
		$config['max_size']    = '102400';//10 MB
		$config['overwrite']	= true;
		$config['remove_spaces'] = true ;
		
		$this->load->library('upload',$config);
		
		
		$this->db->select('d.*,c.category');
		$this->db->from('csv_order_details d');
		 $this->db->join('categories c','c.category_id = d.category_id');
		 $m = $this->db->get()->result_array();
		  //chi lay len HTML 20 cai thui;
			$model = array();
			$i = 0;
			$_quans = 0;$_total =0;
			foreach($m as $it){
				$tt = $it['price'] * $it['amount'];
				$_quans += $it['amount']; 
				$_total += $tt; 
				if($i < 20){
					$model[] = $it;
				}
				$i++;
			}
		$this->data['total_quans'] =  $_quans;
		$this->data['total_money'] =  $_total;
		$this->data['model'] =  $model;
		$this->data['class_step2'] = '';
			
		if($this->upload->do_upload('upload')){
			//xoa het table
			$this->db->empty_table('csv_order_details');
			$this->db->query("ALTER TABLE babi_csv_order_details AUTO_INCREMENT = 1");
			
			$file = $this->upload->data();//file_name
			$file_local = './uploads/'.$file['file_name'];
			//print_r($file);exit;
			$file_path = $file['full_path'];
			//GHI CSV VAO TABLE babi_csv_order_details
			//xóa cache xong mới upload vào database
			
			 //$this->output->clear_all_cache();
			 //echo md5('index.php?/report.html');exit;
			 //$this->output->clear_path_cache('report/table/list');
			 $d = $this->db->query("LOAD DATA LOCAL INFILE '$file_local' 
								INTO TABLE babi_csv_order_details
								CHARACTER SET UTF8
								FIELDS TERMINATED BY ','
								ENCLOSED BY '\"'
								LINES TERMINATED BY '\n'
								IGNORE 1 LINES
								(order_id,item_id,product_id,product_code,price,amount,product,category_id)
			 ");
			 
			 $this->db->select('d.*,c.category');
			 $this->db->from('csv_order_details d');
			 $this->db->join('categories c','c.category_id = d.category_id');
			 $m = $this->db->get()->result_array();
			//chi lay len HTML 20 cai thui;
			$model = array();
			$i = 0;
			$_quans = 0;$_total =0;
			foreach($m as $it){
				$tt = $it['price'] * $it['amount'];
				$_quans += $it['amount']; 
				$_total += $tt; 
				if($i < 20){
					$model[] = $it;
				}
				$i++;
			}
			$this->data['total_quans'] =  $_quans;
			$this->data['total_money'] =  $_total;
			
			$this->data['model'] =  $model;
			$this->data['class_step2'] = '';
			
			
		}
		$post = $this->input->post();
		if(!empty($post['btn_filter'])){
			$filter = $post['filter_type'];
			$tbl = 'csv_order_details';
			if(!empty($post['cat']) && !empty($post['filter_type'])){
				$category_id = $post['cat'];
				//echo "co ca 2";
				$this->db->select('d.product_id,d.product,d.category_id,c.category,sum(d.amount) as amount,d.price');
				$this->db->from('csv_order_details d');
				$this->db->join('categories c','c.category_id = d.category_id');
				$this->db->distinct()->group_by('d.product_id,d.price');
				$this->db->where('d.category_id',$category_id);
				$m = $this->db->get()->result_array();
			}
			elseif(empty($post['cat']) && !empty($post['filter_type'])){
				//echo "chi co filter";
				$this->db->select('d.product_id,d.product_code,d.product,d.category_id,c.category,sum(d.amount) as amount,d.price');
				$this->db->from('csv_order_details d');
				$this->db->join('categories c','c.category_id = d.category_id');
				$this->db->distinct()->group_by('d.product_id,d.price');
				$m = $this->db->get()->result_array();
				//echo $this->db->last_query();exit;
			}
			elseif(!empty($post['cat']) && empty($post['filter_type'])){
				//echo "chi co cat";
				$category_id = $post['cat'];
				$this->db->select('d.order_id,d.product_id,d.product,d.product_code,d.price,d.amount,c.category');
				$this->db->from($tbl.' d');
				$this->db->join('categories c','c.category_id = d.category_id');
				$this->db->where('d.category_id',$category_id);
				$m = $this->db->get()->result_array();
			}
			elseif(empty($post['cat']) && empty($post['filter_type'])){
				//$ti = $t1." TẤT CẢ";
				$this->db->select('d.order_id,d.product_id,d.product,d.product_code,d.price,d.amount,c.category');
				$this->db->from($tbl.' d');
				$this->db->join('categories c','c.category_id = d.category_id');
				$m = $this->db->get()->result_array();
				
			}
			//chi lay len HTML 20 cai thui;
			$model = array();
			$i = 0;
			$_quans = 0;$_total =0;
			foreach($m as $it){
				$tt = $it['price'] * $it['amount'];
				$_quans += $it['amount']; 
				$_total += $tt; 
				if($i < 20){
					$model[] = $it;
				}
				$i++;
			}
			$this->data['total_quans'] =  $_quans;
			$this->data['total_money'] =  $_total;
			$this->data['model'] =  $model;
			$this->data['class_step2'] = '';
			$this->data['category_selected'] = $post['cat'];
			$this->data['type_filter_selected'] = $filter;
		}
		//XUAT FILE
		if(!empty($post['export_detail'])){
			$data_exp = array();
			$query = '';
			$ti = 'file';
			$t1 = "THỐNG KÊ ";
			//print_r($post);exit;//cat , filter_type
			$tbl = 'csv_order_details';
			if(!empty($post['cat']) && !empty($post['filter_type'])){
				$category_id = $post['cat'];
				$this->db->where('category_id',$category_id);
				$ca = $this->db->get('categories')->row();
				$category = $ca->category;
				//echo "co ca 2";
				$ti = $t1." NHÓM THEO SẢN PHẨM CỦA DANH MỤC : ". $category;
				$this->db->select('d.product_id,d.product_code,d.product,c.category,sum(d.amount) as amount,d.price');//,d.category_id
				$this->db->from('csv_order_details d');
				$this->db->join('categories c','c.category_id = d.category_id');
				$this->db->distinct()->group_by('d.product_id,d.price');
				$this->db->where('d.category_id',$category_id);
				$data_exp = $this->db->get()->result_array();
			}
			elseif(empty($post['cat']) && !empty($post['filter_type'])){
				//echo "chi co filter";
				$ti = $t1." NHÓM THEO SẢN PHẨM";
				$this->db->select('d.product_id,d.product_code,d.product,c.category,sum(d.amount) as amount,d.price');//d.category_id,
				$this->db->from('csv_order_details d');
				$this->db->join('categories c','c.category_id = d.category_id');
				$this->db->distinct()->group_by('d.product_id,d.price');
				$data_exp = $this->db->get()->result_array();
			}
			elseif(!empty($post['cat']) && empty($post['filter_type'])){
				//echo "chi co cat";
				$category_id = $post['cat'];
				$this->db->where('category_id',$category_id);
				$ca = $this->db->get('categories')->row();
				$category = $ca->category;
				$ti = $t1."DANH MỤC : ". $category;
				//data
				$this->db->select('d.order_id,d.product_code,d.product_id,d.product,d.price,d.amount,c.category');
				$this->db->from($tbl.' d');
				$this->db->join('categories c','c.category_id = d.category_id');
				$this->db->where('d.category_id',$category_id);
				$data_exp = $this->db->get()->result_array();
			}
			elseif(empty($post['cat']) && empty($post['filter_type'])){
				$ti = $t1." TẤT CẢ";
				$this->db->select('d.order_id,d.product_id,d.product_code,d.product,d.price,d.amount,c.category');
				$this->db->from($tbl.' d');
				$this->db->join('categories c','c.category_id = d.category_id');
				$data_exp = $this->db->get()->result_array();
				
			}
			$this->load->dbutil();
			$query = $this->db->last_query();
			$_query = $this->db->query($query);
			$delimiter = ",";
			$newline = "\r\n";
			$t = $ti.$newline;
			$content_csv  = $this->dbutil->csv_from_result($_query,$delimiter,$newline);
			$file_download = './uploads/file_save.csv';
			$this->load->helper('file');
			write_file($file_download, $t.$content_csv);
			$this->load->helper('download');
			force_download($ti.'.csv',$t.$content_csv);
			
			//print_r($data_exp);
			exit;
		}
		
		//else{ // khi load trang
		
		$cat_ids = array();
		$cat_ids = $this->db->select('category_id,sum(amount) as quantity')->group_by('category_id')->distinct()->get('csv_order_details')->result_array();
		$cat_ids_arr = array();
		$cat_ids_all = array();
		
		$cat_ids_arr[] = 0;
		$cat_ids_all[0] = array('quantity'=>0);
		$qu = 0;
		foreach($cat_ids as $c){
			$cat_ids_arr[] = $c['category_id'];
			$cat_ids_all[$c['category_id']] = array('quantity'=>$c['quantity']);
			$qu += $c['quantity'];
		}
		$cat_ids_all[0] = array('quantity'=>$qu);
		
		$this->db->where_in('category_id',$cat_ids_arr);
		$_categories = $this->db->get('babi_categories')->result_array();
		$babi_categories_array = array();
		foreach($_categories as $cat){
			$babi_categories_array[$cat['category_id']] = $cat['category'] . ' - '.$cat_ids_all[$cat['category_id']]['quantity'];
		}
		$this->data['babi_categories'] = $babi_categories_array;
		//}
		//$this->data['babi_categories'] = $this->categories;
		//print_r($this->categories);exit;
		//echo $post['cat'];exit;
		$this->data['category_selected'] = !empty($post['cat']) ? $post['cat'] : '';
	
		
		
		$this->data['class_step3'] = 'hide';
		$this->data['active_child'] = 'report';
		$this->data['header_title'] = 'Report';
		$this->data['toolbars'] = array(
			array('title'=>'Add','desc'=>'Thêm mới sản phẩm','icon'=>'fa-plus-square','url'=>site_url('products/add')),
			array('title'=>'Refesh','desc'=>'Làm mới','icon'=>'fa-refresh','url'=>'#')
		);
		$this->set_css('css/datepicker.css');
		
		//$this->set_js('vendor/jquery-ui.custom.min.js');
		$this->set_js('js/bootstrap-datepicker.js');
		
		$hook_js = $this->build('report/hooks/js/index',true);
		$html = $this->build('report/table/list',true);
		$this->miniHTML($html);
		$this->miniHTML($hook_js);
		$this->data['content'] = $html;
		$this->data['hook_js'] = $hook_js;
		$this->set_css('vendor/datatables/jquery.dataTables.css');
		$this->set_js('vendor/bootstrap-select/bootstrap-select.js');
		$this->set_js('vendor/datatables/jquery.dataTables.js');
		//$this->set_js('vendor/fuelux/checkbox.js');
		$this->set_js('js/datatables.js');
//		$this->set_js('js/forms.js');
		$this->set_css('vendor/bootstrap-select/bootstrap-select.css');
		$this->build('index');
	}
	
	function miniHTML( &$html ){
		$html = str_replace("\n","",$html);
		$html = str_replace("\t"," ",$html);
		$html = str_replace("   "," ",$html);
		$html = str_replace("    "," ",$html);
		$html = str_replace("     "," ",$html);
		$html = str_replace(">  <","><",$html);
		$html = str_replace("> <","><",$html);
		$html = str_replace(">   ",">",$html);
		$html = str_replace("   <","<",$html);
		$html = str_replace("  <","<",$html);
		$html = preg_replace("/<!--.*?-->/ms","",$html);		
		$html = str_replace("\r","",$html);
		//$html = $this->delete_all_between('/*', '*/', $html);
	}
	/*function delete_all_between($beginning, $end, $string) {
	  $beginningPos = strpos($string, $beginning);
	  $endPos = strpos($string, $end);
	  if ($beginningPos === false || $endPos === false) {
		return $string;
	  }

	  $textToDelete = substr($string, $beginningPos, ($endPos + strlen($end)) - $beginningPos);

	  return str_replace($textToDelete, '', $string);
	}*/
}