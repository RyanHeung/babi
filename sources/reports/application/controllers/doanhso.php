<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Doanhso extends MY_Controller {
	
	public $api_config;
	public $babi_status;
	public $categories;
	//filter
	//$type_filter = array();
	function __construct(){
		
		parent::__construct();
		
		$this->sections = array(
			array('title'=>'Products','url'=>'','action'=>'index'),
		);
		$this->data['menu_active'] = 'doanhso';
		$this->data['breadcrumbs']['doanhso'] = array('title'=>'Doanh số');
		
		$xml=simplexml_load_file("config.xml");
		$this->api_config = $xml->api;
		
		//babi_status
		//$this->babi_status = $this->db->get('babi_status')->result_array();
		//$this->data['babi_status'] = $this->babi_status;
		
		//babi_categories
		$this->categories = $this->db->get('babi_categories')->result_array();
		$babi_categories_array = array();
		foreach($this->categories as $cat){
			$babi_categories_array[$cat['category_id']] = $cat['category'];
		}
		$this->data['babi_categories'] = $babi_categories_array;
		//$this->categories = $babi_categories_array;
		//$this->data['babi_categories'] = $babi_categories_array;
		//print_r($this->data['babi_categories']);exit;
		$this->data['type_filter'] = array(
			''				=> '--------------',
			'group_product'	=> 'Thống kê số lượng bán từng sản phẩm (Product Id )',
			'group_product_code'	=> 'Thống kê số lượng bán từng combination (Product code )',
			//'group_day'	=> 'Thống kê số lượng bán từng ngày'
		);
		
		//$this->output->cache(60000);
		$this->load->library("excel");
		$this->load->helper("excel");
	}
	

	public function index( )
	{
		$file = $this->uri->uri_string();
		//$this->output->cache(60000);
		//echo base_url('report');exit;
		//echo md5(site_url('report'));exit;
		
		
		/*$this->data['category_selected'] = '';
		$this->data['type_filter_selected'] = '';
		$params = $this->input->get();
		
		$this->data['class_step2'] = 'hide';
		
		$folder = "./uploads";
		$config['upload_path'] = $folder;
		$config['allowed_types'] = '*';//doc|docx|zip|rar|jpg|png|xls|xlsx|csv
		$config['max_size']    = '102400';//10 MB
		$config['overwrite']	= true;
		$config['remove_spaces'] = true ;
		
		$this->load->library('upload',$config);
		*/
		
		/*$this->db->select('d.*,c.category');
		$this->db->from('csv_order_details d');
		 $this->db->join('categories c','c.category_id = d.category_id');
		 $m = $this->db->get()->result_array();
		  //chi lay len HTML 20 cai thui;
			$model = array();
			$i = 0;
			$_quans = 0;$_total =0;
			foreach($m as $it){
				$tt = $it['price'] * $it['amount'];
				$_quans += $it['amount']; 
				$_total += $tt; 
				if($i < 20){
					$model[] = $it;
				}
				$i++;
			}
		$this->data['total_quans'] =  $_quans;
		$this->data['total_money'] =  $_total;
		$this->data['model'] =  $model;
		$this->data['class_step2'] = '';
			*/
		/*
		if($this->upload->do_upload('upload')){
			//xoa het table
			$this->db->empty_table('csv_order_details');
			$this->db->query("ALTER TABLE babi_csv_order_details AUTO_INCREMENT = 1");
			
			$file = $this->upload->data();//file_name
			$file_local = './uploads/'.$file['file_name'];
			//print_r($file);exit;
			$file_path = $file['full_path'];
			//GHI CSV VAO TABLE babi_csv_order_details
			//xóa cache xong mới upload vào database
			
			 //$this->output->clear_all_cache();
			 //echo md5('index.php?/report.html');exit;
			 //$this->output->clear_path_cache('report/table/list');
			 $d = $this->db->query("LOAD DATA LOCAL INFILE '$file_local' 
								INTO TABLE babi_csv_order_details
								CHARACTER SET UTF8
								FIELDS TERMINATED BY ','
								ENCLOSED BY '\"'
								LINES TERMINATED BY '\n'
								IGNORE 1 LINES
								(order_id,item_id,product_id,product_code,price,amount,product,category_id)
			 ");
			 
			 $this->db->select('d.*,c.category');
			 $this->db->from('csv_order_details d');
			 $this->db->join('categories c','c.category_id = d.category_id');
			 $m = $this->db->get()->result_array();
			//chi lay len HTML 20 cai thui;
			$model = array();
			$i = 0;
			$_quans = 0;$_total =0;
			foreach($m as $it){
				$tt = $it['price'] * $it['amount'];
				$_quans += $it['amount']; 
				$_total += $tt; 
				if($i < 20){
					$model[] = $it;
				}
				$i++;
			}
			$this->data['total_quans'] =  $_quans;
			$this->data['total_money'] =  $_total;
			
			$this->data['model'] =  $model;
			$this->data['class_step2'] = '';
			
			
		}
		*/
		$post = $this->input->post();
		
		if(!empty($post['export_detail'])){
			$data_exp = array();
			$query = '';
			$ti = 'file';
			$t1 = "THỐNG KÊ DOANH SỐ";
			//print_r($post);exit;//cat , filter_type
			$tbl = 'csv_order_details';
			/*
			if(!empty($post['cat']) && !empty($post['filter_type'])){
				$category_id = $post['cat'];
				$this->db->where('category_id',$category_id);
				$ca = $this->db->get('categories')->row();
				$category = $ca->category;
				//echo "co ca 2";
				$ti = $t1." NHÓM THEO SẢN PHẨM CỦA DANH MỤC : ". $category;
				$this->db->select('d.product_id,d.product_code,d.product,c.category,sum(d.amount) as amount,d.price');//,d.category_id
				$this->db->from('csv_order_details d');
				$this->db->join('categories c','c.category_id = d.category_id');
				$this->db->distinct()->group_by('d.product_id,d.price');
				$this->db->where('d.category_id',$category_id);
				$this->db->where('d.price > ',0);
				$data_exp = $this->db->get()->result_array();
			}
			elseif(empty($post['cat']) && !empty($post['filter_type'])){
				//echo "chi co filter";
				$ty = "Product id";
				if($post['filter_type'] == 'group_product_code')
					$ty = "Product code";
				$ti = $t1." NHÓM THEO SẢN PHẨM - ($ty)";
				$this->db->select('d.product_id,d.product_code,d.product,c.category,sum(d.amount) as amount,d.price');//d.category_id,
				$this->db->from('csv_order_details d');
				$this->db->join('categories c','c.category_id = d.category_id');
				if($post['filter_type'] == 'group_product'){
					$this->db->distinct()->group_by('d.product_id,d.price');
				}
				elseif($post['filter_type'] == 'group_product_code'){
					$this->db->distinct()->group_by('d.product_id,d.product_code,d.price');
					$this->db->order_by('d.product_code');
				}
				$this->db->where('d.price > ',0);
				$data_exp = $this->db->get()->result_array();
			}
			elseif(!empty($post['cat']) && empty($post['filter_type'])){
				//echo "chi co cat";
				$category_id = $post['cat'];
				$this->db->where('category_id',$category_id);
				$ca = $this->db->get('categories')->row();
				$category = $ca->category;
				$ti = $t1."DANH MỤC : ". $category;
				//data
				$this->db->select('d.order_id,d.product_code,d.product_id,d.product,d.price,d.amount,c.category');
				$this->db->from($tbl.' d');
				$this->db->join('categories c','c.category_id = d.category_id');
				$this->db->where('d.category_id',$category_id);
				$this->db->where('d.price > ',0);
				$data_exp = $this->db->get()->result_array();
			}
			elseif(empty($post['cat']) && empty($post['filter_type'])){*/
				$ti = $t1." từ ";
				//123456
				
				/*$this->db->select('d.order_id,d.product_id,d.product_code,d.product,d.price,d.amount,c.category');
				$this->db->from($tbl.' d');
				$this->db->join('categories c','c.category_id = d.category_id');
				$this->db->where('d.price > ',0);
				$data_exp = $this->db->get()->result_array();
				*/
				
			//}
			$this->load->dbutil();
			$query = $this->db->last_query();
			
		//	print_r($data_exp);exit;
			
			$_query = $this->db->query($query);
			$delimiter = ",";
			$newline = "\r\n";
			$t = $ti.$newline;
			$content_csv  = $this->dbutil->csv_from_result($_query,$delimiter,$newline);

			
			//GIANG LAM EXCEL
			$this->array_symbol = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","AA","AB","AC","AD","AE","AF","AG","AH","AI","AJ","AK","AL","AM","AN","AO","AP","AQ","AR","AS","AT","AU","AV","AW","AX","AY","AZ","BA","BB","BC","BD");
			$objPHPExcel = new PHPExcel();
			
			$objPHPExcel->setActiveSheetIndex(0);
			$list_title = array("Tháng","Tuần","Ngày","Category","Amount","Price","SubTotal");
			$symbol_width = array("A"=>10,"B"=>10,"C"=>12,"D"=>10,"E"=>14,"F"=>10,"G"=>10,"I"=>3,"J"=>5,"L"=>5,"N"=>5,"P"=>5,"Q"=>5,"S"=>5,"U"=>5,"V"=>5,"X"=>5,"Z"=>5,"AA"=>5,"AE"=>5,"AF"=>5,"AH"=>5,"AJ"=>5,"AK"=>5,"AM"=>5,"AO"=>5,"AP"=>5,"AR"=>5,"AT"=>5,"AU"=>5,"AW"=>5,"AY"=>5,"AZ"=>5,"BB"=>5,"BD"=>5); // chieu rong từng cot
			header_excel($list_title,$symbol_width,$objPHPExcel,2,true,true);
			$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(25);
			$row = 4;
			$row_start = $row;
			$array_symbol =$this->array_symbol;
			$row_start = 0;
			#$data_products = array_filter($this->db->query($query)->result_array());//data_exp
			//$data_products = array_filter($data_exp);//data_exp
			
			//TITLE
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,1,$t);
			
			$background = !empty($_REQUEST['background']) ? $_REQUEST['background'] : '1EC3C8';//'1EC3C8'; //1EC3C8   1155CC
			$font =  array('bold'=> true, 'color' => array('rgb'=> 'ffffff'),'size'  => 10);
			$styleArray = array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => $background)),'font'=>$font); // mau background
			#$objPHPExcel->getActiveSheet()->mergeCells('A1:F1')->getStyle('A1'.':AG1')->applyFromArray($styleArray)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			
			$objPHPExcel->getActiveSheet()->mergeCells('A1:BD1')->getStyle("A1")->applyFromArray($styleArray)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			
			//set màu sắc hết title
			$objPHPExcel->getActiveSheet()->getStyle("A1:BD3")->applyFromArray($styleArray);
			
			//MERGE nao
			$objPHPExcel->getActiveSheet()->mergeCells('A2:A3')->getStyle("A2")->applyFromArray($styleArray)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->mergeCells('B2:B3')->getStyle("B2")->applyFromArray($styleArray)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->mergeCells('C2:C3')->getStyle("C2")->applyFromArray($styleArray)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			
			
			$objPHPExcel->getActiveSheet()->mergeCells('D2:K2')->getStyle("D2")->applyFromArray($styleArray)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3,2,"Tổng"); // trái 0,1,2,3 và trên xuống 1,2
			//foreach($l = 0;$l<8;$l++){ // tr
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3,3,"Đơn hàng");
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4,3,"Doanh số");
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5,3,"Doanh thu");
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6,3,"Đơn hủy");
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7,3,"Giá trị hủy");
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8,3,"% Hủy");
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9,3,"Đơn sỉ");
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10,3,"DS Sỉ");
			//}
			
			//WEB
			$objPHPExcel->getActiveSheet()->mergeCells('L2:P2')->getStyle("L2")->applyFromArray($styleArray)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11,2,"Web"); // trái 0,1,2,3 và trên xuống 1,2
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11,3,"Đơn hàng");
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12,3,"Doanh số");
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13,3,"Đơn hủy");
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14,3,"Giá trị hủy");
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(15,3,"% hủy");
				
				
			
			//WAP
			$objPHPExcel->getActiveSheet()->mergeCells('Q2:U2')->getStyle("Q2")->applyFromArray($styleArray)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(16,2,"Wap"); // trái 0,1,2,3 và trên xuống 1,2
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(16,3,"Đơn hàng");
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(17,3,"Doanh số");
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(18,3,"Đơn hủy");
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(19,3,"Giá trị hủy");
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(20,3,"% hủy");
			
			
			//TEL
			$objPHPExcel->getActiveSheet()->mergeCells('V2:Z2')->getStyle("V2")->applyFromArray($styleArray)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(21,2,"Tel"); // trái 0,1,2,3 và trên xuống 1,2
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(21,3,"Đơn hàng");
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(22,3,"Doanh số");
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(23,3,"Đơn hủy");
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(24,3,"Giá trị hủy");
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(25,3,"% hủy");
			
			//TEL
			$objPHPExcel->getActiveSheet()->mergeCells('AA2:AE2')->getStyle("AA2")->applyFromArray($styleArray)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(26,2,"FB"); // trái 0,1,2,3 và trên xuống 1,2
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(26,3,"Đơn hàng");
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(27,3,"Doanh số");
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(28,3,"Đơn hủy");
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(29,3,"Giá trị hủy");
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(30,3,"% hủy");
			
			//TEL
			$objPHPExcel->getActiveSheet()->mergeCells('AF2:AJ2')->getStyle("AF2")->applyFromArray($styleArray)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(31,2,"Zopim");
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(31,3,"Đơn hàng");
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(32,3,"Doanh số");
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(33,3,"Đơn hủy");
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(34,3,"Giá trị hủy");
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(35,3,"% hủy");

			//Yahoo
			$objPHPExcel->getActiveSheet()->mergeCells('AK2:AO2')->getStyle("AK2")->applyFromArray($styleArray)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(36,2,"Yahoo");
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(36,3,"Đơn hàng");
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(37,3,"Doanh số");
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(38,3,"Đơn hủy");
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(39,3,"Giá trị hủy");
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(40,3,"% hủy");
			
			
			//Shop
			$objPHPExcel->getActiveSheet()->mergeCells('AP2:AT2')->getStyle("AP2")->applyFromArray($styleArray)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(41,2,"Shop");
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(41,3,"Đơn hàng");
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(42,3,"Doanh số");
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(43,3,"Đơn hủy");
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(44,3,"Giá trị hủy");
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(45,3,"% hủy");
			
			//Tablet
			$objPHPExcel->getActiveSheet()->mergeCells('AU2:AY2')->getStyle("AU2")->applyFromArray($styleArray)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(46,2,"Tablet");
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(46,3,"Đơn hàng");
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(47,3,"Doanh số");
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(48,3,"Đơn hủy");
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(49,3,"Giá trị hủy");
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(50,3,"% hủy");
			
			//chau xac dinh
			$objPHPExcel->getActiveSheet()->mergeCells('AZ2:BD2')->getStyle("AZ2")->applyFromArray($styleArray)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(51,2,"Chưa xác định nguồn mua");
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(51,3,"Đơn hàng");
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(52,3,"Doanh số");
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(53,3,"Đơn hủy");
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(54,3,"Giá trị hủy");
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(55,3,"% hủy");
			
			
			
			//TITLE2
			$font =  array('bold'=> true, 'color' => array('rgb'=> 'ffffff'),'size'  => 10);
			$styleArray = array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => $background)),'font'=>$font); // mau background
			$objPHPExcel->getActiveSheet()->getStyle("A2:G2")->applyFromArray($styleArray)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			
			
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
			
			$doanh_so = array();
			
			#print_r($this->api_config);exit; ->url
			$time_from = $_REQUEST['time_from'];
			$time_to = $_REQUEST['time_to'];
			
			$url = (string)$this->api_config->url."orders3?time_from=$time_from&time_to=$time_to";
			
			$email = "hagiang@blinkb.com";
			$api_key = "1V8A3GvzH4mHUO08l099r4Ji5Aos78jO";
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_USERPWD, $email . ":" . $api_key);
			//curl_setopt($ch, CURLOPT_POSTFIELDS,$order_data);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json'
				//'Content-Length: 1')
			));

			$result = curl_exec($ch);
			#print_r($result);exit;
			$data = json_decode($result,JSON_FORCE_OBJECT);
			curl_close($ch);
			$orders = $data['orders'];
			$params = $data['params'];
			
			
			
			$data_time = array();
			if(!empty($params['time_from'])){
				//echo date('d/m/Y H:i',1414861170);exit; //1414774800  1414861170
				
				/*$s1 = strtotime('11/20/2014 00:00');
				$s2 = strtotime('11/21/2014 00:00');
				echo $s1;echo '<br />';
				echo $s2;echo '<br />';
				exit;*/
				
				$time_from = date("d/m/Y H:i",$params['time_from']);
				
				$time_to = date("d/m/Y H:i",$params['time_to']);
				$ti .= $time_from. ' - '. $time_to;
				$objPHPExcel->getActiveSheet()->mergeCells('A1:AY1')->setCellValueByColumnAndRow(0,1,$ti);
				#$time_test = strtotime('11/02/2014 23:59');
				#echo $time_test;echo '<br />';
			}
			else{
				echo "Chưa chọn thời gian";exit;
			}
			#echo $time_from;echo "<br />";
			#echo $time_to;echo "<br />";
			//print_r($params);exit;
			//1 ngay 86400
			$full_data = array(
				'Total'		=> array('don_hang'=>0,'doanh_so'=>0,'doanh_thu'=>0,'don_huy'=>0,'gia_tri_huy'=>0,'per_huy'=>0,'don_si'=>0,'ds_si'=>0),//
				'Web' 		=> array('don_hang'=>0,'doanh_so'=>0,'don_huy'=>0,'gia_tri_huy'=>0,'per_huy'=>0),//
				'Wap' 		=> array('don_hang'=>0,'doanh_so'=>0,'don_huy'=>0,'gia_tri_huy'=>0,'per_huy'=>0),//
				'Tel' 		=> array('don_hang'=>0,'doanh_so'=>0,'don_huy'=>0,'gia_tri_huy'=>0,'per_huy'=>0),//
				'FB' 		=> array('don_hang'=>0,'doanh_so'=>0,'don_huy'=>0,'gia_tri_huy'=>0,'per_huy'=>0),//
				'Zopim'		=> array('don_hang'=>0,'doanh_so'=>0,'don_huy'=>0,'gia_tri_huy'=>0,'per_huy'=>0),//
				'Yahoo' 	=> array('don_hang'=>0,'doanh_so'=>0,'don_huy'=>0,'gia_tri_huy'=>0,'per_huy'=>0),//
				'Shop' 		=> array('don_hang'=>0,'doanh_so'=>0,'don_huy'=>0,'gia_tri_huy'=>0,'per_huy'=>0),//
				'Tablet' 	=> array('don_hang'=>0,'doanh_so'=>0,'don_huy'=>0,'gia_tri_huy'=>0,'per_huy'=>0),//
			);
			for($s = (int)$params['time_from']; $s < (int)$params['time_to']; $s += 86400){
				$in = $s;
				//get number week
				$ddate = date('Y-m-d',$in);
				$date = new DateTime($ddate);
				$week = $date->format("W");
				
				$data_time[] = array(
					'month'=>date('m',$in),'format'=>date('d/m/Y',$in),'week'=>$week,'time_from'=>$in,'time_to'=>$in+86400,
					'data' => $full_data,
					
				);
			}
			//print_r($data_time);exit;
			
			
			
			//foreach()
			
			
		
			for($i = 0; $i < 30; $i++){
				$doanh_so[] = array(
					'month'=> 1,
					'week'	=> 2,
					'date'	=> '1/1/1990',
				);
			}
			
			
			
			foreach($data_time as $t){
				#if($row==3){
					/*$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,$row,"");
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1,$row,"");
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2,$row,"");
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3,$row,"");
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4,$row,"");
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5,$row,"");
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6,$row,"");*/
				#}else{
					//lay tong so don hang
					$order_in_day = array();
					$doanh_so_in_day = 0;
					$don_huy_in_day = 0;
					$gia_tri_huy_in_day = 0;
					$phan_tram_huy_in_day = 0;
					$don_si_in_day = 0;
					$doanh_so_si_in_day = 0;
					//data_2 : 1 : nguon mua : web
					//WEB
					$web_order_in_day = array();
					$web_tong_don_hang = 0;
					$web_doanh_so = 0;
					$web_don_huy = 0;
					$web_gia_tri_huy = 0;
					$web_phan_tram_huy = 0;
					//WAP
					$wap_order_in_day = array();
					$wap_tong_don_hang = 0;
					$wap_doanh_so = 0;
					$wap_don_huy = 0;
					$wap_gia_tri_huy = 0;
					$wap_phan_tram_huy = 0;
					//TEL
					$tel_order_in_day = array();
					$tel_tong_don_hang = 0;
					$tel_doanh_so = 0;
					$tel_don_huy = 0;
					$tel_gia_tri_huy = 0;
					$tel_phan_tram_huy = 0;
					//Facebook
					$fb_order_in_day = array();
					$fb_tong_don_hang = 0;
					$fb_doanh_so = 0;
					$fb_don_huy = 0;
					$fb_gia_tri_huy = 0;
					$fb_phan_tram_huy = 0;
					
					//Zopim
					$zp_order_in_day = array();
					$zp_tong_don_hang = 0;
					$zp_doanh_so = 0;
					$zp_don_huy = 0;
					$zp_gia_tri_huy = 0;
					$zp_phan_tram_huy = 0;
					
					//Yahoo
					$yh_order_in_day = array();
					$yh_tong_don_hang = 0;
					$yh_doanh_so = 0;
					$yh_don_huy = 0;
					$yh_gia_tri_huy = 0;
					$yh_phan_tram_huy = 0;
					
					//Shop
					$sh_order_in_day = array();
					$sh_tong_don_hang = 0;
					$sh_doanh_so = 0;
					$sh_don_huy = 0;
					$sh_gia_tri_huy = 0;
					$sh_phan_tram_huy = 0;
					
					//Tablet
					$tb_order_in_day = array();
					$tb_tong_don_hang = 0;
					$tb_doanh_so = 0;
					$tb_don_huy = 0;
					$tb_gia_tri_huy = 0;
					$tb_phan_tram_huy = 0;
					
					
					//Tablet
					$khac_order_in_day = array();
					$khac_tong_don_hang = 0;
					$khac_doanh_so = 0;
					$khac_don_huy = 0;
					$khac_gia_tri_huy = 0;
					$khac_phan_tram_huy = 0;
					
					$doanh_thu = 0;
					//print_r($orders);exit;
					foreach($orders as $o){
						if($o['timestamp'] >= $t['time_from'] && $o['timestamp'] < $t['time_to']){
							if($o['status'] == 'R'){
								//print_r($o);exit;
							}
							if($o['status'] == 'C' ){
								$doanh_thu += $o['total'];
							}
							$order_in_day[] = $o;
							$doanh_so_in_day += $o['total'];
							if($o['status']=='I'){
								$don_huy_in_day += 1;
								$gia_tri_huy_in_day += $o['total'];
							}
							$phan_tram_huy_in_day = (float)(($don_huy_in_day * 100)/ count($order_in_day));
							if($o['order_type']=='S'){
								$don_si_in_day += 1;
								$doanh_so_si_in_day += $o['total'];
							}
							//WEB
							if($o['data2'] == 1 && $o['order_type']!='S'){
								$web_order_in_day[] = $o;
								$web_tong_don_hang = count($web_order_in_day);
								$web_doanh_so += $o['total'];
								if($o['status']=='I'){
									$web_don_huy += 1;
									$web_gia_tri_huy  += $o['total'];
									$web_phan_tram_huy = (float)(($web_don_huy * 100)/ count($web_order_in_day));
								}
							}
							//WAP
							if($o['data2'] == 2 && $o['order_type']!='S'){
								$wap_order_in_day[] = $o;
								$wap_tong_don_hang = count($wap_order_in_day);
								$wap_doanh_so += $o['total'];
								if($o['status']=='I'){
									$wap_don_huy += 1;
									$wap_gia_tri_huy  += $o['total'];
									$wap_phan_tram_huy = (float)(($wap_don_huy * 100)/ count($wap_order_in_day));
								}
							}
							//TEL
							if($o['data2'] == 3 && $o['order_type']!='S'){
								$tel_order_in_day[] = $o;
								$tel_tong_don_hang = count($tel_order_in_day);
								$tel_doanh_so += $o['total'];
								if($o['status']=='I'){
									$tel_don_huy += 1;
									$tel_gia_tri_huy  += $o['total'];
									$tel_phan_tram_huy = (float)(($tel_don_huy * 100)/ count($tel_order_in_day));
								}
							}
							//FB
							if($o['data2'] == 4 && $o['order_type']!='S'){
								$fb_order_in_day[] = $o;
								$fb_tong_don_hang = count($fb_order_in_day);
								$fb_doanh_so += $o['total'];
								if($o['status']=='I'){
									$fb_don_huy += 1;
									$fb_gia_tri_huy  += $o['total'];
									$fb_phan_tram_huy = (float)(($fb_don_huy * 100)/ count($fb_order_in_day));
								}
							}
							//Zopim
							if($o['data2'] == 5 && $o['order_type']!='S'){
								$zp_order_in_day[] = $o;
								$zp_tong_don_hang = count($zp_order_in_day);
								$zp_doanh_so += $o['total'];
								if($o['status']=='I'){
									$zp_don_huy += 1;
									$zp_gia_tri_huy  += $o['total'];
									$zp_phan_tram_huy = (float)(($zp_don_huy * 100)/ count($zp_order_in_day));
								}
							}
							//yahoo
							if($o['data2'] == 6 && $o['order_type']!='S'){
								$yh_order_in_day[] = $o;
								$yh_tong_don_hang = count($yh_order_in_day);
								$yh_doanh_so += $o['total'];
								if($o['status']=='I'){
									$yh_don_huy += 1;
									$yh_gia_tri_huy  += $o['total'];
									$yh_phan_tram_huy = (float)(($yh_don_huy * 100)/ count($yh_order_in_day));
								}
							}
							//SHOp
							if($o['data2'] == 8 && $o['order_type']!='S'){//khong tinh sii
								$sh_order_in_day[] = $o;
								$sh_tong_don_hang = count($sh_order_in_day);
								$sh_doanh_so += $o['total'];
								if($o['status']=='I'){
									$sh_don_huy += 1;
									$sh_gia_tri_huy  += $o['total'];
									$sh_phan_tram_huy = (float)(($sh_don_huy * 100)/ count($sh_order_in_day));
								}
							}
							//Tablet
							if($o['data2'] == 37 && $o['order_type']!='S'){
								$tb_order_in_day[] = $o;
								$tb_tong_don_hang = count($tb_order_in_day);
								$tb_doanh_so += $o['total'];
								if($o['status']=='I'){
									$tb_don_huy += 1;
									$tb_gia_tri_huy  += $o['total'];
									$tb_phan_tram_huy = (float)(($tb_don_huy * 100)/ count($tb_order_in_day));
								}
							}
							//nguon mua chua xac dinh 
							if($o['data2'] == 31 && $o['order_type']!='S'){
								$khac_order_in_day[] = $o;
								$khac_tong_don_hang = count($khac_order_in_day);
								$khac_doanh_so += $o['total'];
								if($o['status']=='I'){
									$khac_don_huy += 1;
									$khac_gia_tri_huy  += $o['total'];
									$khac_phan_tram_huy = (float)(($khac_don_huy * 100)/ count($khac_order_in_day));
								}
							}
							
						}
					}
					$total = count($order_in_day);
					if($total > 0):
					$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
					
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,$row,$t['month']);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1,$row,$t['week']);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2,$row,$t['format']);
				
					
					/*
					'Total'		=> array('don_hang'=>0,'doanh_so'=>0,'doanh_thu'=>0,'don_huy'=>0,'gia_tri_huy'=>0,'per_huy'=>0,'don_si'=>0,'ds_si'=>0),//
					'Web' 		=> array('don_hang'=>0,'doanh_so'=>0,'don_huy'=>0,'gia_tri_huy'=>0,'per_huy'=>0),//
					'Wap' 		=> array('don_hang'=>0,'doanh_so'=>0,'don_huy'=>0,'gia_tri_huy'=>0,'per_huy'=>0),//
					'Tel' 		=> array('don_hang'=>0,'doanh_so'=>0,'don_huy'=>0,'gia_tri_huy'=>0,'per_huy'=>0),//
					'FB' 		=> array('don_hang'=>0,'doanh_so'=>0,'don_huy'=>0,'gia_tri_huy'=>0,'per_huy'=>0),//
					'Zopim'		=> array('don_hang'=>0,'doanh_so'=>0,'don_huy'=>0,'gia_tri_huy'=>0,'per_huy'=>0),//
					'Yahoo' 	=> array('don_hang'=>0,'doanh_so'=>0,'don_huy'=>0,'gia_tri_huy'=>0,'per_huy'=>0),//
					'Shop' 		=> array('don_hang'=>0,'doanh_so'=>0,'don_huy'=>0,'gia_tri_huy'=>0,'per_huy'=>0),//
					'Tablet' 	=> array('don_hang'=>0,'doanh_so'=>0,'don_huy'=>0,'gia_tri_huy'=>0,'per_huy'=>0),*/
					//$t['data']['Total']
					
					#print_r($orders);exit;
					//TOTAL
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3,$row,$total);//total
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4,$row,$doanh_so_in_day);
					//$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5,$row,$doanh_thu);	
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6,$row,$don_huy_in_day);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7,$row,$gia_tri_huy_in_day);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8,$row,round($phan_tram_huy_in_day,2));
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9,$row,$don_si_in_day);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10,$row,$doanh_so_si_in_day);
					
					
					//Web
					/*$web_tong_don_hang = 0;
					$web_doanh_so = 0;
					$web_don_huy = 0;
					$web_gia_tri_huy = 0;
					$web_phan_tram_huy = 0;*/
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11,$row,$web_tong_don_hang);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12,$row,$web_doanh_so);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13,$row,$web_don_huy);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14,$row,$web_gia_tri_huy);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(15,$row,round($web_phan_tram_huy,2));
					
					
					//Wap
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(16,$row,$wap_tong_don_hang);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(17,$row,$wap_doanh_so);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(18,$row,$wap_don_huy);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(19,$row,$wap_gia_tri_huy);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(20,$row,round($wap_phan_tram_huy,2));
					
					//TEL
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(21,$row,$tel_tong_don_hang);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(22,$row,$tel_doanh_so);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(23,$row,$tel_don_huy);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(24,$row,$tel_gia_tri_huy);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(25,$row,round($tel_phan_tram_huy,2));
					
					//FB
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(26,$row,$fb_tong_don_hang);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(27,$row,$fb_doanh_so);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(28,$row,$fb_don_huy);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(29,$row,$fb_gia_tri_huy);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(30,$row,round($fb_phan_tram_huy,2));
					
					//Zopim
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(31,$row,$zp_tong_don_hang);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(32,$row,$zp_doanh_so);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(33,$row,$zp_don_huy);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(34,$row,$zp_gia_tri_huy);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(35,$row,round($zp_phan_tram_huy,2));
					
					
					//Yahoo
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(36,$row,$yh_tong_don_hang);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(37,$row,$yh_doanh_so);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(38,$row,$yh_don_huy);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(39,$row,$yh_gia_tri_huy);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(40,$row,round($yh_phan_tram_huy,2));
					
					
					//Shop
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(41,$row,$sh_tong_don_hang);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(42,$row,$sh_doanh_so);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(43,$row,$sh_don_huy);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(44,$row,$sh_gia_tri_huy);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(45,$row,round($sh_phan_tram_huy,2));
					
					//Tablet
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(46,$row,$tb_tong_don_hang);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(47,$row,$tb_doanh_so);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(48,$row,$tb_don_huy);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(49,$row,$tb_gia_tri_huy);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(50,$row,round($tb_phan_tram_huy,2));
					
					//Khác
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(51,$row,$khac_tong_don_hang);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(52,$row,$khac_doanh_so);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(53,$row,$khac_don_huy);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(54,$row,$khac_gia_tri_huy);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(55,$row,round($khac_phan_tram_huy,2));
					
				#}
				
				
				
				/*
				
				*/
				$i = 0;
				$row_start = $row;
				$row++;
				endif;
				$objPHPExcel->getActiveSheet()->freezePane('D4');
			}
			
			//K
			$objPHPExcel->getActiveSheet()->getStyle('K4:K'.$row)->getNumberFormat()->setFormatCode('#,###');
			
			//doanh so
			$objPHPExcel->getActiveSheet()->getStyle('E4:E'.$row)->getNumberFormat()->setFormatCode('#,###');
			//gia tri huy
			$objPHPExcel->getActiveSheet()->getStyle('H4:H'.$row)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->getStyle('M4:M'.$row)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->getStyle('O4:O'.$row)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->getStyle('H4:H'.$row)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->getStyle('R4:R'.$row)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->getStyle('T4:T'.$row)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->getStyle('W4:W'.$row)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->getStyle('Y4:Y'.$row)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->getStyle('AB4:AB'.$row)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->getStyle('AD4:AD'.$row)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->getStyle('AG4:AG'.$row)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->getStyle('AI4:AI'.$row)->getNumberFormat()->setFormatCode('#,###');
			
			$objPHPExcel->getActiveSheet()->getStyle('AL4:AL'.$row)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->getStyle('AG4:AG'.$row)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->getStyle('AI4:AI'.$row)->getNumberFormat()->setFormatCode('#,###');
			
			$objPHPExcel->getActiveSheet()->getStyle('AN4:AN'.$row)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->getStyle('AQ4:AQ'.$row)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->getStyle('AS4:AS'.$row)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->getStyle('AV4:AV'.$row)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->getStyle('AX4:AX'.$row)->getNumberFormat()->setFormatCode('#,###');
			
			$objPHPExcel->getActiveSheet()->getStyle('BA4:BA'.$row)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->getStyle('BC4:BC'.$row)->getNumberFormat()->setFormatCode('#,###');
			//%
			$format_percent = array(
				'numberformat' => array(
					'code' => '0.00%',
				),
			);
			$objPHPExcel->getActiveSheet()->getStyle('I4:I'.$row)->getNumberFormat()->applyFromArray($format_percent);
			
			
			
			$objPHPExcel->getActiveSheet()->getStyle('F2:F'.$row)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->getStyle('G2:G'.$row)->getNumberFormat()->setFormatCode('#,###');
			$styleArray = array('font' => array('bold'=> false, 'color' => array('rgb1'=> '000'),'size'  => 10));
			$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':AG'.$row)->applyFromArray($styleArray);	
			
			//style
			$font =  array('bold'=> true, 'color' => array('rgb'=> '24A7F4'),'size'  => 10);
			$styleArray = array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'ccffcc')),'font'=>$font); // mau background
			//SUM tong don hang
			$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($styleArray)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$row,'=SUM(D4:D'.$row_start.')');
			
			//SUM tong don si
			$objPHPExcel->getActiveSheet()->getStyle('J'.$row)->applyFromArray($styleArray)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->setCellValue('J'.$row,'=SUM(J4:J'.$row_start.')');
			//DOANH SO SI
			$objPHPExcel->getActiveSheet()->getStyle('K'.$row)->applyFromArray($styleArray)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->setCellValue('K'.$row,'=SUM(K4:K'.$row_start.')');
			
			
			
			$objPHPExcel->getActiveSheet()->getStyle('L'.$row)->applyFromArray($styleArray)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->setCellValue('L'.$row,'=SUM(L4:L'.$row_start.')');
			
			$objPHPExcel->getActiveSheet()->getStyle('M'.$row)->applyFromArray($styleArray)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->setCellValue('M'.$row,'=SUM(M4:M'.$row_start.')');
			
			$objPHPExcel->getActiveSheet()->getStyle('N'.$row)->applyFromArray($styleArray)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->setCellValue('N'.$row,'=SUM(N4:N'.$row_start.')');
			
			$objPHPExcel->getActiveSheet()->getStyle('O'.$row)->applyFromArray($styleArray)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->setCellValue('O'.$row,'=SUM(O4:O'.$row_start.')');
			
			$objPHPExcel->getActiveSheet()->getStyle('Q'.$row)->applyFromArray($styleArray)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->setCellValue('Q'.$row,'=SUM(Q4:Q'.$row_start.')');
			$objPHPExcel->getActiveSheet()->getStyle('R'.$row)->applyFromArray($styleArray)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->setCellValue('R'.$row,'=SUM(R4:R'.$row_start.')');
			
			$objPHPExcel->getActiveSheet()->getStyle('T'.$row)->applyFromArray($styleArray)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->setCellValue('T'.$row,'=SUM(T4:T'.$row_start.')');
			
			$objPHPExcel->getActiveSheet()->getStyle('V'.$row)->applyFromArray($styleArray)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->setCellValue('V'.$row,'=SUM(V4:V'.$row_start.')');
			$objPHPExcel->getActiveSheet()->getStyle('W'.$row)->applyFromArray($styleArray)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->setCellValue('W'.$row,'=SUM(W4:W'.$row_start.')');
			
			$objPHPExcel->getActiveSheet()->getStyle('X'.$row)->applyFromArray($styleArray)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->setCellValue('X'.$row,'=SUM(X4:X'.$row_start.')');
			$objPHPExcel->getActiveSheet()->getStyle('Y'.$row)->applyFromArray($styleArray)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->setCellValue('Y'.$row,'=SUM(Y4:Y'.$row_start.')');
			
			$objPHPExcel->getActiveSheet()->getStyle('AA'.$row)->applyFromArray($styleArray)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->setCellValue('AA'.$row,'=SUM(AA4:AA'.$row_start.')');
			$objPHPExcel->getActiveSheet()->getStyle('AB'.$row)->applyFromArray($styleArray)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->setCellValue('AB'.$row,'=SUM(AB4:AB'.$row_start.')');
			$objPHPExcel->getActiveSheet()->getStyle('AC'.$row)->applyFromArray($styleArray)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->setCellValue('AC'.$row,'=SUM(AC4:AC'.$row_start.')');
			$objPHPExcel->getActiveSheet()->getStyle('AD'.$row)->applyFromArray($styleArray)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->setCellValue('AD'.$row,'=SUM(AD4:AD'.$row_start.')');
			
			$objPHPExcel->getActiveSheet()->getStyle('AF'.$row)->applyFromArray($styleArray)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->setCellValue('AF'.$row,'=SUM(AF4:AF'.$row_start.')');
			$objPHPExcel->getActiveSheet()->getStyle('AG'.$row)->applyFromArray($styleArray)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->setCellValue('AG'.$row,'=SUM(AG4:AG'.$row_start.')');
			$objPHPExcel->getActiveSheet()->getStyle('AH'.$row)->applyFromArray($styleArray)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->setCellValue('AH'.$row,'=SUM(AH4:AH'.$row_start.')');
			$objPHPExcel->getActiveSheet()->getStyle('AI'.$row)->applyFromArray($styleArray)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->setCellValue('AI'.$row,'=SUM(AI4:AI'.$row_start.')');
			
			$objPHPExcel->getActiveSheet()->getStyle('AK'.$row)->applyFromArray($styleArray)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->setCellValue('AK'.$row,'=SUM(AK4:AK'.$row_start.')');
			$objPHPExcel->getActiveSheet()->getStyle('AL'.$row)->applyFromArray($styleArray)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->setCellValue('AL'.$row,'=SUM(AL4:AL'.$row_start.')');
			$objPHPExcel->getActiveSheet()->getStyle('AM'.$row)->applyFromArray($styleArray)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->setCellValue('AM'.$row,'=SUM(AM4:AM'.$row_start.')');
			$objPHPExcel->getActiveSheet()->getStyle('AN'.$row)->applyFromArray($styleArray)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->setCellValue('AN'.$row,'=SUM(AN4:AN'.$row_start.')');
			
			$objPHPExcel->getActiveSheet()->getStyle('AP'.$row)->applyFromArray($styleArray)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->setCellValue('AP'.$row,'=SUM(AP4:AP'.$row_start.')');
			$objPHPExcel->getActiveSheet()->getStyle('AQ'.$row)->applyFromArray($styleArray)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->setCellValue('AQ'.$row,'=SUM(AQ4:AQ'.$row_start.')');
			$objPHPExcel->getActiveSheet()->getStyle('AR'.$row)->applyFromArray($styleArray)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->setCellValue('AR'.$row,'=SUM(AR4:AR'.$row_start.')');
			$objPHPExcel->getActiveSheet()->getStyle('AS'.$row)->applyFromArray($styleArray)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->setCellValue('AS'.$row,'=SUM(AS4:AS'.$row_start.')');
			
			$objPHPExcel->getActiveSheet()->getStyle('AU'.$row)->applyFromArray($styleArray)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->setCellValue('AU'.$row,'=SUM(AU4:AU'.$row_start.')');
			$objPHPExcel->getActiveSheet()->getStyle('AV'.$row)->applyFromArray($styleArray)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->setCellValue('AV'.$row,'=SUM(AV4:AV'.$row_start.')');
			$objPHPExcel->getActiveSheet()->getStyle('AW'.$row)->applyFromArray($styleArray)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->setCellValue('AW'.$row,'=SUM(AW4:AW'.$row_start.')');
			$objPHPExcel->getActiveSheet()->getStyle('AX'.$row)->applyFromArray($styleArray)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->setCellValue('AX'.$row,'=SUM(AX4:AX'.$row_start.')');
			
			$objPHPExcel->getActiveSheet()->getStyle('S'.$row)->applyFromArray($styleArray)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->setCellValue('S'.$row,'=SUM(S4:S'.$row_start.')');
			
			
			$objPHPExcel->getActiveSheet()->getStyle('AZ'.$row)->applyFromArray($styleArray)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->setCellValue('AZ'.$row,'=SUM(AZ4:AZ'.$row_start.')');
			$objPHPExcel->getActiveSheet()->getStyle('BA'.$row)->applyFromArray($styleArray)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->setCellValue('BA'.$row,'=SUM(BA4:BA'.$row_start.')');
			$objPHPExcel->getActiveSheet()->getStyle('BB'.$row)->applyFromArray($styleArray)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->setCellValue('BB'.$row,'=SUM(BB4:BB'.$row_start.')');
			$objPHPExcel->getActiveSheet()->getStyle('BC'.$row)->applyFromArray($styleArray)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->setCellValue('BC'.$row,'=SUM(BC4:BC'.$row_start.')');
			
			
			
			
			//SUM AMOUNT
			$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($styleArray)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$row,'=SUM(E3:E'.$row_start.')');
			//SUM total
			$objPHPExcel->getActiveSheet()->getStyle('G'.$row)->applyFromArray($styleArray)->getNumberFormat()->setFormatCode('#,###');
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$row,'=SUM(G3:G'.$row_start.')');
			
			//MAU SAC CHO DE NHIN
			$_font =  array('size'  => 10);
			$_styleArray = array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'D8D8D8')),'font'=>$_font); // mau background
			for($j = 4;$j < $row; $j++){
				if($j%2 == 0){
					$objPHPExcel->getActiveSheet()->getStyle('A'.$j.':BD'.$j)->applyFromArray($_styleArray);
				}
			}
			
			header('Content-Type: application/vnd.ms-excel'); 
			header('Content-Disposition: attachment;filename="'.$ti.'.xls"'); 
			header('Cache-Control: max-age=0'); 
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5'); 
			$objWriter->save('php://output');
			return;
			//print_r($data_products);exit;
			 //= array_filter($data_employ);
			
			
			
			
			
			
			
			$file_download = './uploads/file_save.csv';
			$this->load->helper('file');
			write_file($file_download, $t.$content_csv);
			$this->load->helper('download');
			force_download($ti.'.csv',$t.$content_csv);
			
			//print_r($data_exp);
			exit;
		}
		
		//else{ // khi load trang
		
		$cat_ids = array();
		$cat_ids = $this->db->select('category_id,sum(amount) as quantity')->group_by('category_id')->distinct()->get('csv_order_details')->result_array();
		$cat_ids_arr = array();
		$cat_ids_all = array();
		
		$cat_ids_arr[] = 0;
		$cat_ids_all[0] = array('quantity'=>0);
		$qu = 0;
		foreach($cat_ids as $c){
			$cat_ids_arr[] = $c['category_id'];
			$cat_ids_all[$c['category_id']] = array('quantity'=>$c['quantity']);
			$qu += $c['quantity'];
		}
		$cat_ids_all[0] = array('quantity'=>$qu);
		
		$this->db->where_in('category_id',$cat_ids_arr);
		$_categories = $this->db->get('babi_categories')->result_array();
		$babi_categories_array = array();
		foreach($_categories as $cat){
			$babi_categories_array[$cat['category_id']] = $cat['category'] . ' - '.$cat_ids_all[$cat['category_id']]['quantity'];
		}
		$this->data['babi_categories'] = $babi_categories_array;
		//}
		//$this->data['babi_categories'] = $this->categories;
		//print_r($this->categories);exit;
		//echo $post['cat'];exit;
		$this->data['category_selected'] = !empty($post['cat']) ? $post['cat'] : '';
	
		
		
		$this->data['class_step3'] = 'hide';
		$this->data['active_child'] = 'doanhso';
		$this->data['header_title'] = 'Doanh số';
		$this->data['toolbars'] = array(
			array('title'=>'Add','desc'=>'Thêm mới sản phẩm','icon'=>'fa-plus-square','url'=>site_url('products/add')),
			array('title'=>'Refesh','desc'=>'Làm mới','icon'=>'fa-refresh','url'=>'#')
		);
		$this->set_css('css/datepicker.css');
		
		//$this->set_js('vendor/jquery-ui.custom.min.js');
		$this->set_js('js/bootstrap-datepicker.js');
		
		$hook_js = $this->build('doanhso/hooks/js/index',true);
		$html = $this->build('doanhso/table/list',true);
		$this->miniHTML($html);
		$this->miniHTML($hook_js);
		$this->data['content'] = $html;
		$this->data['hook_js'] = $hook_js;
		$this->set_css('vendor/datatables/jquery.dataTables.css');
		$this->set_js('vendor/bootstrap-select/bootstrap-select.js');
		$this->set_js('vendor/datatables/jquery.dataTables.js');
		//$this->set_js('vendor/fuelux/checkbox.js');
		$this->set_js('js/datatables.js');
//		$this->set_js('js/forms.js');
		$this->set_css('vendor/bootstrap-select/bootstrap-select.css');
		$this->build('index');
	}
	
	function miniHTML( &$html ){
		$html = str_replace("\n","",$html);
		$html = str_replace("\t"," ",$html);
		$html = str_replace("   "," ",$html);
		$html = str_replace("    "," ",$html);
		$html = str_replace("     "," ",$html);
		$html = str_replace(">  <","><",$html);
		$html = str_replace("> <","><",$html);
		$html = str_replace(">   ",">",$html);
		$html = str_replace("   <","<",$html);
		$html = str_replace("  <","<",$html);
		$html = preg_replace("/<!--.*?-->/ms","",$html);		
		$html = str_replace("\r","",$html);
		//$html = $this->delete_all_between('/*', '*/', $html);
	}
	/*function delete_all_between($beginning, $end, $string) {
	  $beginningPos = strpos($string, $beginning);
	  $endPos = strpos($string, $end);
	  if ($beginningPos === false || $endPos === false) {
		return $string;
	  }

	  $textToDelete = substr($string, $beginningPos, ($endPos + strlen($end)) - $beginningPos);

	  return str_replace($textToDelete, '', $string);
	}*/
}