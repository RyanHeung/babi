<?php if(!empty($breadcrumbs)):?>
<div class="breadcrumbs" id="breadcrumbs">
	<script type="text/javascript">
		try { ace.settings.check('breadcrumbs', 'fixed') } catch (e) { }
	</script>

	<ul class="breadcrumb col-sm-6">
		<li><i class="fa fa-home"></i><a href="<?php echo base_url();?>">Home</a></li>
		<?php foreach($breadcrumbs as $b) :
			
		?>
		<li <?php if(empty($b['url'])) echo "class = \"active\" ";?>>
			<?php if(!empty($b['url'])){?><a href="<?php echo $b['url']?>"><?php }?>
			<?php echo $b['title'] ?>
			<?php if(!empty($b['url'])){?></a><?php }?>
		</li>
		<?php endforeach;?>
	</ul>
	<?php if(!empty($toolbars)):?>
	<ul id="toolbar-nav" class="nav nav-pills pull-right collapse navbar-collapse col-sm-6">
		<?php foreach($toolbars as $t):?>
		<li><a class="toolbar_btn btn-help" href="<?php echo $t['url']?>" title="<?php echo $t['desc']?>"><i class="fa <?php echo $t['icon']?>"></i><div><?php echo $t['title']?></div></a></li>
		<?php endforeach;?>
	</ul>
	<?php endif;?>
	<div class="clearfix"></div>
</div>
<?php endif;?>