<!--GIANG NE-->
<div class="form-group text-left">
	<?php echo form_open_multipart('');?>
	<label class="control-label col-sm-3" for="Text1" title="Product name"><span>Upload file *.csv</span></label>
	<div class="col-sm-4">
		<div class="form-group">
			<input type="file" name="upload" />
		</div>
	</div>
<div class="clearfix"></div>
<div class="form-group text-left">

		<button type="submit" href="javascript:;" id="btn-step1" class="btn btn-success btn-rounded" value="get">Xem</button>

</div>
<div class="clearfix"></div>
<!--<button type="submit" href="javascript:;" class="btn btn-success btn-rounded <?php echo $class_step2 ;?> " value="save" name="save">Lưu</button>-->
<button type="submit" href="javascript:;" class="btn btn-success btn-rounded <?php echo $class_step3 ;?>" value="save" name="export_detail">Xuất file</button>
<div class="clearfix"></div>


</div>
<hr />
<?php //echo form_open(site_url('report/export'));?>
<div class="form-group text-left">
	<div class="col-sm-3">
	<?php 
	//echo $category_selected.">";
	echo form_dropdown('cat',$babi_categories,$category_selected,'class="categories '.$class_step2.'"')?>
	</div>
	<div class="col-sm-4">
<?php 
	echo form_dropdown('filter_type',$type_filter,$type_filter_selected,'style="width:200px" class="filter '.$class_step2.'"')?>
	</div>
	<div class="col-sm-4">
		<button type="submit" href="javascript:;" class="btn btn-success btn-rounded" value="btn_filter" name="btn_filter">Lọc</button>
		<button type="submit" href="javascript:;" class="btn btn-success btn-rounded" value="save" name="export_detail">Xuất file</button>
	</div>
</div>	
<?php echo form_close();?>
<?php //echo form_close();?>


                            <div class="table-responsive no-border">
                                <table class="table table-bordered table-striped mg-t datatable">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Order id</th>
                                            <th>Tên hàng</th>
                                            <th>Số lượng</th>
                                            <th>Đơn giá</th>
											<th>Thành tiền</th>
											<th>Danh mục</th>
                                        </tr>
                                    </thead>
                                    <tbody>	
										<?php $_quans = 0;$_total =0; if(!empty($model)): $i=1;?>
										<?php foreach($model as $item):?>
                                        <tr>
                                            <td><?php echo $i;$i++; ?></td>
                                            <td>#<?php echo !empty($item['order_id']) ? $item['order_id'] : '';?></td>
											<td><?php echo $item['product'];?></td>
											<td class="text-right"><?php echo $item['amount'];?></td>
											<td class="text-right"><?php echo number_format($item['price'],0,',','.');?></td>
											<?php $tt = $item['price'] * $item['amount'];
													$_quans += $item['amount']; 
													$_total += $tt; 
											?>
                                            <td class="text-right"><?php echo number_format($tt,0,',','.');?></td>
											<td><?php 
												echo $item['category'];
											//echo form_dropdown('cat',$babi_categories,$item['category_id'],'disabled class="disabled"')?></td>
                                        </tr>
										<?php endforeach;?>
										<?php endif;?>
                                    </tbody>
                                    <tfoot>
                                        <!--<tr>
                                            <th>No.</th>
                                            <th>Order id</th>
                                            <th>Tên hàng</th>
                                            <th>Số lượng</th>
                                            <th>Đơn giá</th>
											<th>Thành tiền</th>
                                        </tr>-->
										<tr>
                                            <th></th>
                                            <th></th>
                                            <th class="text-right">Tổng số lượng</th>
                                            <th class="text-right"><?php echo number_format($total_quans,0,',','.');?></th>
                                            <th class="text-right">Tổng tiền</th>
											<th class="text-right"><?php echo number_format($total_money,0,',','.');?></th>
											<td></td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>