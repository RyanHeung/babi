<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, user-scalable=1, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<link rel="icon" type="image/x-icon" href="<?php echo img_url()?>favicon.ico">
	<meta name="robots" content="NOFOLLOW, NOINDEX">
	<title>Babi.vn</title>
	<link rel="stylesheet" href="<?php echo asset_url()?>bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?php echo css_url()?>skins/palette.1.css" id="skin" />
    <link rel="stylesheet" href="<?php echo css_url()?>main.css" />
    <link rel="stylesheet" href="<?php echo css_url()?>animate.min.css" />
    <link rel="stylesheet" href="<?php echo asset_url()?>vendor/offline/theme.css" />
    <link rel="stylesheet" href="<?php echo css_url()?>fonts/style.1.css" id="font" />
	<!--[if lt IE 9]><script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script><script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script><![endif]-->
	
	<link rel="stylesheet" href="<?php echo css_url()?>panel.css">
    <script src="<?php echo asset_url()?>vendor/modernizr.js"></script>
</head>
<body  class="ps_back-office page-sidebar  admindashboard">
<?php echo $this->load->view("_header");?>
<?php echo $this->load->view("_content_login");?>
<?php echo $this->load->view("_footer");?>
    <script>(function (d, e, j, h, f, c, b) { d.GoogleAnalyticsObject = f; d[f] = d[f] || function () { (d[f].q = d[f].q || []).push(arguments) }, d[f].l = 1 * new Date(); c = e.createElement(j), b = e.getElementsByTagName(j)[0]; c.async = 1; c.src = h; b.parentNode.insertBefore(c, b) })(window, document, "script", "//www.google-analytics.com/analytics.js", "ga"); ga("create", "UA-50530436-1", "nyasha.me"); ga("send", "pageview");</script>
</body>
</html>