<?php 
//
$active = !empty($menu_active) ? $menu_active : "dashboard";
$navigation = array(
	'dashboard' => array(
		'title'	=> 'Dashboard',
		'icon'	=> 'fa-dashboard',
		'url'	=> base_url(),
	),
	/*'catalog' => array(
		'title'	=> 'Catalog',
		'icon'	=>'fa-database',
		'url'	=> site_url('catalog'),
		'items'=>array(
			array('action'=>'products','title'=>'Products','url'=>site_url('products')),
			array('action'=>'packages','title'=>'Packages','url'=>site_url('packages')),
			array('action'=>'tours','title'=>'Tours','url'=>site_url('tours')),
			array('action'=>'services','title'=>'Services','url'=>site_url('services'))	
		)
	),*/
	/*
	'sales' => array(
		'title'	=> 'Sales',
		'icon'	=>'fa-money',
		'url'	=> site_url('sales'),	
	),*/
	'report' => array(
		'title'	=> 'Report',
		'icon'	=>'fa-money',
		'url'	=> site_url('report'),	
	),
	'doanhso' => array(
		'title'	=> 'Doanh số',
		'icon'	=>'fa-thumbs-o-up',
		'url'	=> site_url('doanhso'),	
	),
	'offline' => array(
		'title'	=> 'Offline',
		'icon'	=>'fa-money',
		'url'	=> site_url('offline'),	
	),
	'trahang' => array(
		'title'	=> 'Trả hàng',
		'icon'	=>'fa-reply-all',
		'url'	=> site_url('trahang'),	
	),
	'plugins' => array(
		'title'	=> 'Plugins',
		'icon'	=>'fa-database',
		'url'	=> site_url('plugins'),	
	),
	
	'users' => array(
		'title'	=> 'Thoát',
		'icon'	=>'fa-users',
		'url'	=> site_url('users/logout')
	),
	/*'payments' => array(
		'title'	=> 'Payments',
		'icon'	=>'fa-dollar',
		'url'	=> site_url('payments'),	
	),*/
	/*'users' => array(
		'title'	=> 'Users',
		'icon'	=>'fa-users',
		'url'	=> site_url('users'),
		'items'=>array(
			array('action'=>'client','title'=>'Client','url'=>site_url('Client')),
			array('action'=>'collaborator','title'=>'Collaborator','url'=>site_url('collaborator')),
			array('action'=>'roles','title'=>'Roles','url'=>site_url('roles')),
			array('action'=>'area','title'=>'Area','url'=>site_url('area')),
			array('action'=>'groups','title'=>'Groups','url'=>site_url('groups'))			
		)
	),*/
	/*
	'config' => array(
		'title'	=> 'Config',
		'icon'	=>'fa-cog',
		'url'	=> site_url('config'),	
	),
	*/
);
//phan quyen
$role = $this->session->userdata("role");
if($role == 'babi1'){
	unset($navigation['trahang']);
	unset($navigation['doanhso']);
	unset($navigation['offline']);	
}
?>
<aside class="sidebar canvas-left">
<nav class="main-navigation">
	<ul>
		<?php foreach($navigation as $key=>$nav):
			$_act = "";
			if($key == $active){
				$_act = "active";
			}
		?>
		<li class="dropdown <?php echo $_act;?> show-on-hover">
			<!--data-toggle="dropdown"-->
			<a href="<?php echo $nav['url'];?>"><i class="fa <?php echo $nav['icon']?>"></i><span><?php echo $nav["title"]?></span></a>
			<?php if(!empty($nav['items'])): ?>
			<ul class="dropdown-menu">
			<?php foreach($nav['items'] as $item):
				$a = "";
				if(!empty($active_child)){
					if($active_child == $item['action'])
						$a = "class=\" active \"";
				}
			?>
				<li <?php echo $a; ?> ><a href="<?php echo $item['url'];?>"><span><?php echo $item['title'];?></span></a></li>
				<?php endforeach;?>
			</ul>
			<?php endif;?>
			
		</li>
		<?php endforeach;?>
	</ul>
</nav>
</aside>