<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, user-scalable=1, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<link rel="icon" type="image/x-icon" href="<?php echo img_url()?>favicon.ico">
	<meta name="robots" content="NOFOLLOW, NOINDEX">
	<title>Babi.vn</title>
	<link rel="stylesheet" href="<?php echo asset_url()?>bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?php echo css_url()?>skins/palette.1.css" id="skin" />
    <link rel="stylesheet" href="<?php echo css_url()?>main.css" />
    <link rel="stylesheet" href="<?php echo css_url()?>animate.min.css" />
    <link rel="stylesheet" href="<?php echo asset_url()?>vendor/offline/theme.css" />
    <link rel="stylesheet" href="<?php echo css_url()?>fonts/style.1.css" id="font" />
	<!--[if lt IE 9]><script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script><script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script><![endif]-->
	
	<link rel="stylesheet" href="<?php echo css_url()?>panel.css">
	<?php foreach($css_files as $css=>$v):?>
		<link rel="stylesheet" href="<?php echo asset_url().$css;?>" id="font" />
	<?php endforeach;?>
    <script src="<?php echo asset_url()?>vendor/modernizr.js"></script>
	
	<!--<script type="text/javascript" src="<?php echo js_url()?>jquery-1.11.0.min.js"></script>
	<script type="text/javascript" src="<?php echo js_url()?>jquery-migrate-1.2.1.min.js"></script>
	-->
</head>
<body  class="ps_back-office page-sidebar  admindashboard">
<?php echo $this->load->view("_header");?>
<?php echo $this->load->view("_content");?>
<?php echo $this->load->view("_footer");?>
<!--<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds</p>-->
	<script src="<?php echo asset_url()?>vendor/jquery-1.11.1.min.js"></script>
	<script src="<?php echo asset_url()?>bootstrap/js/bootstrap.js"></script>
    <script src="<?php echo js_url()?>off-canvas.js"></script>
    <script src="<?php echo asset_url()?>vendor/jquery.placeholder.js"></script>
    <script src="<?php echo asset_url()?>vendor/offline/offline.min.js"></script>
    <script src="<?php echo asset_url()?>vendor/pace/pace.min.js"></script>
    <script src="<?php echo js_url()?>main.js"></script>
    <script src="<?php echo js_url()?>panel.js"></script>
	<?php foreach($js_files as $js=>$v):?>
		<script src="<?php echo asset_url().$js;?>"></script>
	<?php endforeach;?>
	<?php if(!empty($hook_js)){
		echo $hook_js;
	}
	?>
    <script>(function (d, e, j, h, f, c, b) { d.GoogleAnalyticsObject = f; d[f] = d[f] || function () { (d[f].q = d[f].q || []).push(arguments) }, d[f].l = 1 * new Date(); c = e.createElement(j), b = e.getElementsByTagName(j)[0]; c.async = 1; c.src = h; b.parentNode.insertBefore(c, b) })(window, document, "script", "//www.google-analytics.com/analytics.js", "ga"); ga("create", "UA-50530436-1", "nyasha.me"); ga("send", "pageview");</script>
</body>
</html>