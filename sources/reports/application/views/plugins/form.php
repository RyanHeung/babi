<div class="col-sm-12 inner">
    <form name="mainForm" action="#" method="post">
        <div class="col-sm-12 nopadding-left">
            <div class="tab-content">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="t_client_first_name" title="First Name">
                            <span>First Name</span></label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="t_client_first_name" />
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="t_client_last_name" title="Last Price"><span>Last Name</span></label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="t_client_last_name" />
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="t_client_passport" title="Passport"><span>Passport</span></label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="t_client_passport" />
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="t_client_id_card" title="Id Card"><span>Id Card</span></label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="t_client_id_card" />
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="t_client_student_card" title="Student Card"><span>Student Card</span></label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="t_client_student_card" />
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="t_client_birthday" title="Birthday"><span>Birthday</span></label>
                        <div class="col-sm-7">
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-calendar-o"></i></div>
                                <input class="form-control datepicker" type="text" placeholder="Choose date" id="t_client_birthday">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="t_client_email" title="Email"><span>Email</span></label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="t_client_email" />
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="t_client_gender" title="Gender"><span>Gender</span></label>
                        <div class="col-sm-4">
                            <select class="form-control" id="t_client_gender">
                                <option>Male</option>
                                <option>Female</option>
                                <option>Orther</option>
                            </select>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="t_client_remark" title="Remark"><span>Remark</span></label>
                        <div class="col-sm-7">
                            <textarea class="form-control" id="t_client_remark" rows="3"></textarea>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="t_client_occupancy" title="Occupancy"><span>Occupancy</span></label>
                        <div class="col-sm-4">
                            <select class="form-control" id="t_client_occupancy">
                                <option>Occupancy 001</option>
                                <option>Occupancy 002</option>
                                <option>Occupancy 003</option>
                                <option>Occupancy 004</option>
                            </select>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="t_client_country" title="Gender"><span>Country</span></label>
                        <div class="col-sm-4">
                            <select class="form-control" id="t_client_country">
                                <option>U.S.A</option>
                                <option>Mexico</option>
                                <option>Brazil</option>
                                <option>Cuba</option>
                            </select>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="clearfix"></div>

            </div>
        </div>
    </form>
</div>
