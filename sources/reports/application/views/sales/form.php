<form role="form" class="form-horizontal parsley-form" action="add.html" method="post">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">First name</label><div class="col-sm-4">
                                        <input type="text" class="form-control" name="firstname" data-parsley-required="true" data-parsley-trigger="change" placeholder="First Name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Last name</label><div class="col-sm-4">
                                        <input type="text" class="form-control" name="lastname" data-parsley-required="true" data-parsley-trigger="change" placeholder="Last Name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Country</label><div class="col-sm-4">
                                        <input type="text" class="form-control" name="country" data-parsley-required="true" data-parsley-trigger="change" placeholder="Country">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">State/Province</label><div class="col-sm-4">
                                        <input type="text" class="form-control" name="state" data-parsley-required="true" data-parsley-trigger="change" placeholder="State/Province">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">City</label><div class="col-sm-4">
                                        <input type="text" class="form-control" name="city" data-parsley-required="true" data-parsley-trigger="change" placeholder="City">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Zipcode</label><div class="col-sm-4">
                                        <input type="text" class="form-control" name="zipcode" data-parsley-type="alphanum" data-parsley-required="true" data-parsley-trigger="change" placeholder="P6B6L4">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Home phone</label><div class="col-sm-4">
                                        <input type="text" class="form-control" name="phone" data-parsley-type="digits" data-rangelength="[11,20]" data-parsley-required="true" data-parsley-trigger="change" placeholder="18005551234">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Mobile phone</label><div class="col-sm-4">
                                        <input type="text" class="form-control" name="mobilephone" data-parsley-type="digits" data-rangelength="[11,20]" data-parsley-required="true" data-parsley-trigger="change" placeholder="18005551234">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Email</label><div class="col-sm-4">
                                        <input type="text" class="form-control" name="email" data-parsley-type="email" data-parsley-required="true" data-parsley-trigger="change" placeholder="hello@nyasha.me">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Website</label><div class="col-sm-4">
                                        <input type="text" class="form-control" name="website" data-parsley-type="url" data-parsley-required="true" data-parsley-trigger="change" placeholder="http://nyasha.me">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"></label>
                                    <div class="col-sm-4">
                                        <button class="btn btn-primary btn-parsley">Submit</button>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Textarea</label><div class="col-sm-8">
                                        <textarea class="form-control" rows="3"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">CKeditor</label><div class="col-sm-8">
                                        <textarea id="ckeditor" class="form-control" rows="3"></textarea>
                                    </div>
                                </div>
                            </form>

							