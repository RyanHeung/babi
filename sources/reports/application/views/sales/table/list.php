
<div class="form-group text-left">
	<?php echo form_open();?>
	<label class="control-label col-sm-3" for="Text1" title="Product name"><span>Chọn thời gian</span></label>
	<div class="col-sm-4">
		<div class="form-group">
			<div class="input-group input-daterange">
				<div class="input-group-addon"><i class="fa fa-calendar-o"></i></div>
				<input name="start" class="form-control datepicker" type="text" value="<?php echo $start;?>" placeholder="Từ ngày">
				<div class="input-group-addon"><i class="fa fa-calendar-o"></i></div>
				<input name="end" class="form-control datepicker2" type="text" value="<?php echo $end;?>" placeholder="Đến ngày">
			</div>
		</div>
	</div>
<div class="clearfix"></div>
<hr />
<div>
<?php foreach($babi_status as $status):?>
<label>
	<input <?php if(!empty($status['checked'])) echo "checked";?> type="checkbox" value="<?php echo $status['status']?>" name="status[]" />
	<?php echo $status['description']?>
</label> | 
<?php endforeach;?>
</div>
<div class="clearfix"></div>
<div class="form-group text-left">

		<button type="submit" href="javascript:;" id="btn-step1" class="btn btn-success btn-rounded" value="get">Xem</button>

</div>
<div class="clearfix"></div>
<div class="col-sm-4"><input type="text" value="<?php echo $title ;?>" name="title" class="form-control <?php echo $class_step2 ;?>" placeholder="Tên report" /></div>
<button type="submit" href="javascript:;" class="btn btn-success btn-rounded <?php echo $class_step2 ;?> " value="save" name="save">Lưu Đơn hàng</button>
<button type="submit" href="javascript:;" class="btn btn-success btn-rounded <?php echo $class_step3 ;?>" value="save" name="export_detail">Xuất file</button>
<div class="clearfix"></div>
<?php echo form_close();?>

</div>
<hr />



                            <div class="table-responsive no-border">
                                <table class="table table-bordered table-striped mg-t datatable1">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Order id</th>
                                            <th>Ngày mua</th>
                                            <th>Tổng tiền</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>	
										<?php if(!empty($model)): $i=1;?>
										<?php foreach($model as $item):?>
                                        <tr>
                                            <td><?php echo $i;$i++; ?></td>
                                            <td>#<?php echo $item['order_id'];?></td>
                                            <td><?php echo date("d/m/Y H:i",$item['timestamp']);?></td>
                                            <td><?php echo number_format($item['total'],0,',','.');?></td>
                                            <td>
                                                <div class="action-buttons fa-hover">
                                                   <a class="text-primary" href="#" title="Edit"><i class="fa fa-eye"></i> Chi tiết</a>
                                                </div>
                                            </td>
                                        </tr>
										<?php endforeach;?>
										<?php endif;?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>No.</th>
                                            <th>Order id</th>
                                            <th>Ngày mua</th>
                                            <th>Tổng tiền</th>
                                            <th></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>