<div class="app">
        <header class="header header-fixed navbar">
            <div class="brand"><a href="javascript:;" class="fa fa-bars off-left visible-xs" data-toggle="off-canvas" data-move="ltr"></a><a href="index.html" class="navbar-brand text-white"><i class="fa fa-stop mg-r-sm"></i><span class="heading-font">Babi<b> REPORT</b></span></a></div>
        </header>
        <section class="layout">
            <?php echo $this->load->view('_navigation')?>
            <section class="main-content">
                <div class="content-wrap">
					<section class="panel panel-info">
						<?php echo $this->load->view("_breadcrumbs");?>
						<?php if(!empty($header_title)) {?>
						<header class="panel-heading"><?php echo $header_title;?></header>
						<?php } ?>
						<div class="panel-body">
							<!--@message-->
							<?php if(!empty($notifications_form)):
								foreach( $notifications_form as $no): 
								$_class = "";
								if($no['type']=="E")
									$_class = "alert-danger";
								if($no['type']=="S")
									$_class = "alert-success";
								
							?>
							<div class="alert <?php echo $_class;?>"> 
								<?php echo $no['message']?>
							</div>
							<?php endforeach; endif;?>
							<?php echo $content;?>
						</div>
					</section>
				</div>
            </section>
        </section>
    </div>