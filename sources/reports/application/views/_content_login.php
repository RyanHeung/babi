<div class="app">
        <header class="header header-fixed navbar">
            <div class="brand"><a href="javascript:;" class="fa fa-bars off-left visible-xs" data-toggle="off-canvas" data-move="ltr"></a><a href="index.html" class="navbar-brand text-white"><i class="fa fa-stop mg-r-sm"></i><span class="heading-font">Babi<b> REPORT</b></span></a></div>
        </header>
        <section class="layout">
            <aside class="sidebar canvas-left">
				<nav class="main-navigation">
					<ul>
					</ul>
				</nav>
				</aside>
            <section class="main-content">
                <div class="content-wrap">
					<section class="panel panel-info">
						<?php if(!empty($header_title)) {?>
						<header class="panel-heading"><?php echo $header_title;?></header>
						<?php } ?>
						<div class="panel-body">
							<!--@message-->
							<?php if(!empty($notifications_form)):
								foreach( $notifications_form as $no): 
								$_class = "";
								if($no['type']=="E")
									$_class = "alert-danger";
								if($no['type']=="S")
									$_class = "alert-success";
								
							?>
							<div class="alert <?php echo $_class;?>"> 
								<?php echo $no['message']?>
							</div>
							<?php endforeach; endif;?>
							<div class="col-sm-12 inner">
							<?php echo form_open('');?>
							<div class="col-sm-12 nopadding-left">
								 <div class="col-sm-12">
									<div class="form-group">
										<label class="control-label col-sm-3" for="username" title="Username">
											<span>Username</span></label>
										<div class="col-sm-5">
											<input type="text" class="form-control" name="username"/>
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-3" for="password" title="Password">
											<span>Password</span></label>
										<div class="col-sm-5">
											<input type="password" class="form-control" name="password"/>
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-3" title=""></label>
										<div class="col-sm-5">
											<input class="btn btn-success btn-rounded" type="submit" value="Login" />
										</div>
									</div>
									
								</div>
							</div>
							<?php echo form_close();?>
							</div>
						</div>
					</section>
				</div>
            </section>
        </section>
    </div>