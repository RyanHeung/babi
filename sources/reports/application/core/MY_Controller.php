<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller{
	public $data;
	public $sections = array();
	function __construct(){
		parent::__construct();
		$this->data=array(
			'title'			=> '',
//			'header_title'	=>
			'content'			=> '',
			'js_files'		=> array(),
			'css_files'		=> array(),
			'menu_active'	=> 'dashboard',
			'sections'		=> $this->sections
		);
		$logined =  $this->session->userdata('logined');
		if(empty($logined)){
			redirect(site_url('users/login'));
		}
		date_default_timezone_set('Asia/Saigon');
	}
	
	function build( $view ,$layout = false ){
		//if($layout==true)
			//return $this->load->view($view,$this->data,$layout);
		return $this->load->view($view,$this->data,$layout);
	}
	function set_css($file){
		$this->data['css_files'][$file] = true;
	}
	function set_js($file){
		$this->data['js_files'][$file] = true;
	}
}
?>